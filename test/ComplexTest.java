import junit.framework.TestCase;
import org.junit.Test;
public class ComplexTest extends TestCase {


        Complex a =new Complex(3.0,4.0);
        Complex b =new Complex( 2.0,-4.0);
        Complex c =new Complex(0.0,0.0);
        Complex d =new Complex(-3.0,0.0);
        Complex e =new Complex(-6.0,-1.0);
    @Test
    public  void testgetRealPart()throws Exception{
        assertEquals(3.0,a.getRealPart());
        assertEquals(2.0,b.getRealPart());
        assertEquals(0.0,c.getRealPart());
        assertEquals(-3.0,d.getRealPart());
        assertEquals(-6.0,e.getRealPart());
    }
    @Test
    public void testgetImagePart()throws Exception{
        assertEquals(4.0,a.getImagePart());
        assertEquals(-4.0,b.getImagePart());
        assertEquals(0.0,c.getImagePart());
        assertEquals(0.0,d.getImagePart());
        assertEquals(-1.0,e.getImagePart());

    }



    @Test
        public void testtoString()throws Exception{
            assertEquals("3.0+4.0i",a.toString());
            assertEquals("2.0-4.0i",b.toString());
            assertEquals("0.0",c.toString());
            assertEquals("-3.0",d.toString());
            assertEquals("-6.0-1.0i",e.toString());
        }
        @Test
        public void testComplexAdd()throws Exception{
            assertEquals("5.0",a.ComplexAdd(b).toString());
            assertEquals("2.0-4.0i",b.ComplexAdd(c).toString());
            assertEquals("-1.0-4.0i",b.ComplexAdd(d).toString());
        }
        @Test
        public void testComplexSub()throws Exception{
            assertEquals("1.0+8.0i",a.ComplexSub(b).toString());
            assertEquals("-2.0+4.0i",c.ComplexSub(b).toString());
            assertEquals("3.0",c.ComplexSub(d).toString());
        }
        @Test
        public void testComplexMulti()throws Exception{
            assertEquals("22.0-4.0i",a.ComplexMulti(b).toString());
            assertEquals("0.0",b.ComplexMulti(c).toString());
            assertEquals("18.0+3.0i",d.ComplexMulti(e).toString());
        }
        @Test
        public void testComplexDiv()throws Exception{
            assertEquals("-0.2-0.5i",a.ComplexDiv(b).toString());
            assertEquals("0.0",c.ComplexDiv(b).toString());
        }
        @Test
        public void testequals()throws Exception{
            assertEquals(true,a.equals(a));
            assertEquals(false,a.equals(b));

        }
    }

