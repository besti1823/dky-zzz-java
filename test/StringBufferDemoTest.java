import junit.framework.TestCase;
import org.junit.Test;

public class StringBufferDemoTest extends TestCase {

        StringBuffer a = new StringBuffer("StringBuffer");
        StringBuffer b = new StringBuffer("HelloJava");
        StringBuffer c = new StringBuffer("Tested by 20182304");
        @Test
        public void testCharAt() throws Exception{
            assertEquals('S',a.charAt(0));
            assertEquals('a',b.charAt(6));
            assertEquals('b',c.charAt(7));
        }
        @Test
        public void testcapacity() throws Exception{
            assertEquals(28,a.capacity());
            assertEquals(25,b.capacity());
            assertEquals(34,c.capacity());


        }
        @Test
        public void testlength() throws Exception{
            assertEquals(12,a.length());
            assertEquals(9,b.length());
            assertEquals(18,c.length());
        }
        @Test
        public void testindexOf(){
            assertEquals(6,a.indexOf("Buff"));
            assertEquals(0,b.indexOf("He"));
            assertEquals(14,c.indexOf("23"));
        }


    }