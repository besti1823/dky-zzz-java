import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

class Node {
    Node left;
    Node right;
    char val;

    public Node() {
        this.left = null;
        this.right = null;
    }

    public Node(char val) {
        this.left = null;
        this.right = null;
        this.val = val;
    }
}

//AB#CD###E#F##
public class TreeBinary {
    static Node root = new Node();
    static char ch1[];
    static int i = 0;
    //构建二叉树(#代表空结点)

    static Node CreateTree() {
        Node node = null;
        if (ch1[i] == '#') {
            node = null;
            i++;
        } else {
            node = new Node();
            node.val = ch1[i];
            i++;
            node.left = CreateTree();
            node.right = CreateTree();
        }
        return node;


    }

    //先序遍历

    public static void preOrder(Node root) {
        if (root != null) {
            System.out.print(root.val + " ");
            preOrder(root.left);
            preOrder(root.right);
        }
    }

    //中序遍历

    public static void inOrder(Node root) {
        if (root != null) {
            inOrder(root.left);
            System.out.print(root.val + " ");
            inOrder(root.right);
        }
    }

    //后序遍历

    public static void postOrder(Node root) {
        if (root != null) {
            postOrder(root.left);
            postOrder(root.right);
            System.out.print(root.val + " ");
        }
    }

    //层次遍历


    public static void levOrder(Node root) {
        if (root != null) {
            Node p = root;
            Queue<Node> queue = new LinkedList<>();
            queue.add(p);
            while (!queue.isEmpty()) {
                p = queue.poll();
                System.out.print(p.val + " ");
                if (p.left != null) {
                    queue.add(p.left);
                }
                if (p.right != null) {
                    queue.add(p.right);
                }
            }
        }
    }

    //非递归前序
    public static void preOrderTraverse(Node root) {
        Stack<Node> stack = new Stack<>();
        Node node1 = root;
        while (node1 != null || !stack.empty()) {
            if (node1 != null) {
                System.out.print(node1.val + " ");
                stack.push(node1);
                node1 = node1.left;
            } else {
                Node tem = stack.pop();
                node1 = tem.right;
            }
        }
    }

    //非递归中序
    public static void inOrderTraverse(Node root) {
        Stack<Node> stack = new Stack<>();
        Node node1 = root;
        while (node1 != null || !stack.isEmpty()) {
            if (node1 != null) {
                stack.push(node1);
                node1 = node1.left;
            } else {
                Node tem = stack.pop();
                System.out.print(tem.val + " ");
                node1 = tem.right;
            }
        }
    }


    //计算叶子节点的个数

    public static int getSize(Node root) {
        if (root == null) {
            return 0;
        }
        if (root.left == null && root.right == null) {
            return 1;
        } else {
            return getSize(root.left) + getSize(root.right);//递归
        }
    }

    //计算二叉树的高度

    public static int getHeight(Node root) {
        if (root == null) {
            return 0;
        }
        int leftHeight = getHeight(root.left);
        int rightHeight = getHeight(root.right);
        return (leftHeight > rightHeight) ? (leftHeight + 1) : (rightHeight + 1);
    }

  /*  public static void main(String[] args) {
        System.out.println("输入为：AB#CD###E#F##");
        Node root = CreateTree();
        System.out.println("\n二叉树叶子结点数：" + getSize(root));
        System.out.println("二叉树高度：" + getHeight(root));

        System.out.println("\n层次遍历：");
        levOrder(root);
        System.out.println("\n递归先序遍历：");
        preOrder(root);

        System.out.println("\n递归中序遍历：");
        inOrder(root);

        System.out.println("\n递归后序遍历：");
        postOrder(root);

        System.out.println("\n非递归先序遍历：");
        preOrderTraverse(root);

        System.out.println("\n非递归中序遍历：");
        inOrderTraverse(root);

    }*/
}
