package src2;


public class ContactSE implements Comparable {
    private String Name, phone;

    public ContactSE(String Name, String phone) {
        this.Name = Name;

        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Name:" + Name + ";  phone:" + phone;
    }

    @Override
    public int compareTo(Object o) {
        int result;
        result = phone.compareTo(((ContactSE) o).phone);
        return result;
    }
}

