package src2;

public class RunSearch {
    private static ContactSE[] players = new ContactSE[3];

    public static void main(String[] args) {

        players[0] = new ContactSE("Wang", "610-555-7384");
        players[1] = new ContactSE("Wu", "215-555-3827");
        players[2] = new ContactSE("Zhang", "733-555-2969");


        ContactSE searchElement1 = new ContactSE("Wu", "215-555-3827");
        ContactSE searchElement2 = new ContactSE("", "610-555-7384");

        //线性
        System.out.println("LinearSearch1:" + "\n" + LinearSearch(searchElement1));
        System.out.println("LinearSearch2:" + "\n" + LinearSearch(searchElement2));

        //二分
        System.out.println("BinarySearch1:" + "\n" + BinarySearch(searchElement1));
        System.out.println("BinarySearch2:" + "\n" + BinarySearch(searchElement2));
    }

    public static String LinearSearch(ContactSE searchElement) {
        boolean SearchSuccess = false;
        String result = "";
        for (int i = 0; i < players.length; i++) {
            if (players[i].compareTo(searchElement) == 0) {
                result = "Found! [" + players[i].toString() + "]";
                SearchSuccess = true;
                break;
            }
        }

        if (SearchSuccess == false) {
            result = "Not Found!";
        }
        return result;
    }

    public static String BinarySearch(ContactSE searchElement) {
        //排序
        for (int i = 0; i < players.length - 1; i++) {
            for (int j = i + 1; j < players.length; j++) {
                if (players[i].compareTo(players[j]) > 0) {
                    ContactSE temp = players[i];
                    players[i] = players[j];
                    players[j] = temp;
                }
            }
        }

        //查找
        int high, low, middle;
        low = 0;
        high = players.length - 1;
        middle = (low + high) / 2;
        boolean searchSuccess = false;
        String result = "";

        while (searchSuccess == false) {
            if (low > high) {
                searchSuccess = false;
                result = "Not Found!";
                break;
            } else {
                if (players[middle].compareTo(searchElement) == 0) {
                    searchSuccess = true;
                    result = "Found! [" + players[middle].toString() + "]";
                    break;
                } else if (searchElement.compareTo(players[middle]) < 0) {
                    high = middle - 1;
                } else if (searchElement.compareTo(players[middle]) > 0) {
                    low = middle + 1;
                }
                middle = (low + high) / 2;
            }
        }
        return result;
    }
}

