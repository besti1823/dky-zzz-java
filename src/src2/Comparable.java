package src2;

public interface Comparable {

    public String toString();

    public int compareTo(Object o);
}
