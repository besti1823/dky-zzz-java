package src2;

public class Contact implements Comparable {
    private String firstName, lastName, phone;

    //-----------------------------------------------------------------
    //  Sets up this contact with the specified information.
    //-----------------------------------------------------------------
    public Contact(String first, String last, String telephone) {
        firstName = first;
        lastName = last;
        phone = telephone;
    }


    public String toString() {
        return lastName + ", " + firstName + ":  " + phone;
    }


    public int compareTo(Object other) {
        int result;

        if (lastName.equals(((Contact) other).lastName))
            result = firstName.compareTo(((Contact) other).firstName);
        else
            result = lastName.compareTo(((Contact) other).lastName);

        return result;
    }
}
