import java.text.DecimalFormat;
import java.util.Scanner;
public class work35
{
public static void main (String[] args)
{
int radius;
double area , circum;
Scanner scan = new Scanner(System.in);
System.out.print("Please entre the circle's radius: ");
radius = scan.nextInt();
area = Math.PI * Math.pow(radius,2);
circum = 2* Math.PI * radius ;
DecimalFormat fmt = new DecimalFormat("0.###");
System.out.println("The circle area : " + fmt.format(area) );
System.out.println("The circle's circumference: "
+ fmt.format(circum));
}
}


