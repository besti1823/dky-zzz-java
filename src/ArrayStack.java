import java.util.Arrays;

public class ArrayStack<T> implements Stack<T> {
    private final int DEFAULT_Capacity = 3;
    private int count = 0;
    private T[] array;

    public ArrayStack() {

        this.array = (T[]) (new Object[DEFAULT_Capacity]);
        this.count = 0;
    }

    public void expandCapacity() {
        array = Arrays.copyOf(array, array.length * 2);
    }


    @Override
    public T pop() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("Stack element empty");
        T element = (T) this.array[count - 1];
        count--;
        T result = array[count];
        array[count] = null;
        return result;

    }

    @Override
    public void push(T element) {
        if (count == array.length) {
            expandCapacity();
        }
        array[count] = element;
        count++;
    }


    @Override
    public T peek() throws IllegalAccessException {
        if (isEmpty())
            throw new IllegalAccessException("stack element empty");
        return array[this.count - 1];
    }

    @Override
    public boolean isEmpty() {
        if (count == 0)
            return true;
        if (array[count - 1] == null)
            return true;
        else
            return false;
    }

    public int size() {
        return count;
    }

    public String toString() {
        String result = "<top of stack>\n";
        for (int index = count - 1; index >= 0; index--) {
            result += array[index] + "\n";
        }
        return result + "<bottom of stack>";
    }
}

