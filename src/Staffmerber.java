abstract public class Staffmerber {
    protected String name;
    protected String address;
    protected String phone;

    public Staffmerber(String name, String address, String phone) {
        this.name = name;
        this.address = address;
        this.phone = phone;
    }

    @Override//打印有限信息
    public String toString() {
        String result = "Name: " + name + "\n";
        result += "Address: " + address + "\n";
        result += "Phone: " + phone;
        return result;
    }

    public abstract double pay();

    public abstract int holiday();
//定义返回数组中数据，并打印，还有抽象方法PAY，每个子类都会用到
}
