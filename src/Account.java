import java.text.NumberFormat;
public class Account {
    private final double RATE = 0.035;
    private String name;
    private long accNumber;
    private  double banlance;

    public Account(String name, long accNumber, double banlance) {
        this.name = name;
        this.accNumber = accNumber;
        this.banlance = banlance;
    }
    public double deposit (double amount)
    {
        if(amount > 0)
        {
            banlance = banlance + amount;
            return banlance;
        }
        return amount;
    }
    public double withdraw (double amount , double fee)
    {
        if(amount + fee >0 && amount + fee < banlance) {
            banlance = banlance - amount - fee;
        }

            return banlance;

    }

    public double addInterest ()
    {
        banlance +=(banlance*RATE);
        return banlance;
    }

    public double getBanlance ()
    {
        return banlance;
    }

    @Override
    public String toString() {
        NumberFormat fmt = NumberFormat.getCurrencyInstance();
        return "Account{" +
                "name='" + name + '\'' +
                ", accNumber=" + accNumber +
                ", banlance=" + fmt.format(banlance) +
                '}';
    }
}

