

public class Coin1 {
    enum Con{HEADS,TAILS}
    public static void main(String[] args) {
        final int FLIPS = 1000;
         int num1 = 0;
        int num2 = 0;
        int count=0;
         Coin2 myCoin1 = new Coin2();
        for(count = 0;count<FLIPS;count++)
        {
            myCoin1.flip();
            if(myCoin1.isHeads()== Coin2.Con.HEADS)
                num1++;
            else num2++;
        }
        System.out.println("Number of flips: " + FLIPS);
        System.out.println("Number of heads: " + num1);
        System.out.println("Number of tails: " + num2);
    }
}
