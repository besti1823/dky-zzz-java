package Haffman;

import java.io.*;
import java.util.*;

public class HaffmanTree {

    private static char[] ch = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', ' '};

    private char[] a = new char[100];

    private int[] sum = new int[27];

    private HaffNode root;

    private String[] strings = new String[28];//保存编码

    private LinkedList treelist = new LinkedList<HaffNode>();//列表


    public LinkedList createTree() {
        count();
        //匹配
        for (int i = 0; i < 27; i++) {
            HaffNode node = new HaffNode();
            node.setCh(ch[i]);
            node.setN(sum[i]);
            treelist.add(i, node);
        }
        Collections.sort(treelist);
        while (treelist.size() > 1) {
            //获得两个权值最小的节点
            HaffNode first = (HaffNode) treelist.removeFirst();
            HaffNode second = (HaffNode) treelist.removeFirst();
            //将这两个权值最小的节点构造成父节点
            HaffNode parent = new HaffNode();
            parent.setN(first.getN() + second.getN());
            parent.setLeft(first);
            parent.setRight(second);
            //把父节点添加进列表，并重新排序
            treelist.add(parent);
            Collections.sort(treelist);
        }
        root = (HaffNode) treelist.getFirst();
        return treelist;
    }

    //根据哈夫曼树获得各节点编码
    public void getCode(HaffNode root, String code) {
        if (root.getLeft() != null) {
            getCode(root.getLeft(), code + "0");
        }
        if (root.getRight() != null) {
            getCode(root.getRight(), code + "1");
        }
        if (root.getRight() == null && root.getLeft() == null) {
            System.out.println(root.getCh() + "的频次是" + root.getN() + "，编码是" + code);
            root.setCode(code);
        }
    }

    public void read(String address, String name) throws IOException {

        int i = 0;
        File f = new File(address, name);
        Reader reader = new FileReader(f);
        while (reader.ready()) {
            a[i++] = (char) reader.read();
        }
        reader.close();

        System.out.print("压缩前：");
        for (int k = 0; k < a.length; k++) {
            System.out.print(a[k]);
        }
        System.out.println();
    }

    public void count() {

        for (int k = 0; k < a.length; k++) {
            for (int j = 0; j < ch.length; j++) {
                if (a[k] == ch[j])
                    sum[j]++;
            }
        }
    }

    //文件压缩
    public void Compress(String path) throws IOException {
        String result = "";
        for (int i = 0; i < 27; i++) {
            result += ch[i] + "" + sum[i] + ",";//？
        }
        String content = "";
        for (int i = 0; i < a.length; i++) {
            for (int k = 0; k < ch.length; k++) {
                if (a[i] == ch[k])
                    content += search(root, ch[k]).getCode() + " ";
            }
        }
        result += content;
        File f = new File(path);
        if (!f.exists()) {
            f.createNewFile();
        }
        System.out.println("编码结果：" + content);
        Writer writer = new FileWriter(f);
        BufferedWriter bufferedWriter = new BufferedWriter(writer);
        bufferedWriter.write(result);
        bufferedWriter.flush();
        bufferedWriter.close();
    }

    public HaffNode search(HaffNode root, char c) {
        if (root.getCh() == c) {
            return root;
        }
        if (root.getLeft() != null || root.getRight() != null) {
            HaffNode a = search(root.getLeft(), c);
            HaffNode b = search(root.getRight(), c);
            if (a != null)
                return a;
            if (b != null)
                return b;
        }
        return null;
    }

    public HaffNode getRoot() {
        return root;
    }

    public void read2(String address, String name) throws IOException {
        //读取文件
        File file = new File(address, name);
        Reader reader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader((new InputStreamReader(new FileInputStream(file), "GBK")));
        String str = "";
        String temp = "";
        while ((temp = bufferedReader.readLine()) != null) {
            System.out.println("压缩后文件内容：" + temp);
            str = temp;
        }
        //获取每个字符的频数(使用逗号分割)
        StringTokenizer s = new StringTokenizer(str, ",");
        int i = 0;
        while (s.hasMoreTokens()) {
            strings[i++] = s.nextToken();
        }
    }

    public LinkedList createTree2() {
        for (int i = 0; i < 27; i++) {
            HaffNode temp = new HaffNode();
            temp.setCh(strings[i].charAt(0));
            temp.setN(strings[i].charAt(1) - '0');
            treelist.add(temp);
        }
        Collections.sort(treelist);
        while (treelist.size() > 1) {
            //获得两个权值最小的节点
            HaffNode first = (HaffNode) treelist.removeFirst();
            HaffNode second = (HaffNode) treelist.removeFirst();
            //将这两个权值最小的节点构造成父节点
            HaffNode parent = new HaffNode();
            parent.setN(first.getN() + second.getN());
            parent.setLeft(first);
            parent.setRight(second);
            //把父节点添加进列表，并重新排序
            treelist.add(parent);
            Collections.sort(treelist);
        }
        root = (HaffNode) treelist.getFirst();
        return treelist;
    }

    public void reCompress(String path) throws IOException {
        String t = strings[27];
        String result = "";
        StringTokenizer stringTokenizer = new StringTokenizer(t);
        while (stringTokenizer.hasMoreTokens()) {
            String temp = stringTokenizer.nextToken();
            result += search2(root, temp).getCh();
        }
        System.out.println("解码后：" + result);

        File f = new File(path);
        Writer writer = new FileWriter(f);
        BufferedWriter bufferedWriter = new BufferedWriter(writer);
        bufferedWriter.write(result);
        bufferedWriter.flush();
        bufferedWriter.close();
    }

    public HaffNode search2(HaffNode root, String code) {
        if (root.getCode() == null) {
            if (root.getLeft() != null || root.getRight() != null) {
                HaffNode a = search2(root.getLeft(), code);
                HaffNode b = search2(root.getRight(), code);
                if (a != null)
                    return a;
                if (b != null)
                    return b;
            }
            return null;
        } else if (root.getCode().equals(code)) {
            return root;
        }
        return null;
    }
}