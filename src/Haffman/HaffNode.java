package Haffman;

public class HaffNode implements Comparable {
    private HaffNode left, right, father;
    private char ch;
    private int n;
    private String code;

    public void setLeft(HaffNode left) {
        this.left = left;
    }

    public void setRight(HaffNode right) {
        this.right = right;
    }

    public void setFather(HaffNode father) {
        this.father = father;
    }

    public void setCh(char ch) {
        this.ch = ch;
    }


    public void setN(int sum) {
        this.n = sum;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public HaffNode getLeft() {
        return left;
    }

    public HaffNode getRight() {
        return right;
    }


    public char getCh() {
        return ch;
    }

    public int getN() {
        return n;
    }

    public String getCode() {
        return code;
    }

    public int compareTo(Object o) {
        HaffNode a = (HaffNode) o;
        if (this.n > a.n)
            return 1;
        else if (this.n == a.n)
            return 0;
        else
            return -1;
    }


}
