import java.util.Stack;
import java.util.Scanner;

public class Stackwork2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Stackwork3 calcu = new Stackwork3();
        String str1 = null;
        int num1 = 0;
        System.out.println("Enter: 7    4    -3    *    1    5    +    /    *\n");
        str1 = scan.nextLine();

        System.out.println("Entered: " + str1);
        num1 = calcu.calculate(str1);
        System.out.println("Result " + num1);
    }
}
