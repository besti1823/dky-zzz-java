import java.util.Scanner;

public class newjisuan2 implements jisuanComplex {
    private double RealPart1;
    private double ImagePart1;
    private double RealPart2;
    private double ImagePart2;

    public newjisuan2(double R1, double I1, double R2, double I2) {
        this.RealPart1 = R1;
        this.ImagePart1 = I1;
        this.RealPart2 = R2;
        this.ImagePart2 = I2;

    }

    public String ComplexAdd() {
        double R, I;
        R = RealPart1 + RealPart2;
        I = ImagePart1 + ImagePart2;
        String s = null;
        if (I > 0) {
            s = R + "+" + I + "i";
        }
        if (I == 0) {
            s = R + "";
        }
        if (I < 0) {
            s = R + "-" + I * (-1) + "i";
        }
        return s;

    }

    public String ComplexSub() {
        double R, I;
        R = RealPart1 - RealPart2;
        I = ImagePart1 - ImagePart2;
        String s = null;
        if (I > 0) {
            s = R + "+" + I + "i";
        }
        if (I == 0) {
            s = R + "";
        }
        if (I < 0) {
            s = R + "-" + I * (-1) + "i";
        }
        return s;

    }

    public String ComplexMulti() {
        double R, I;
        R = RealPart1 * RealPart2 - ImagePart1 * ImagePart2;
        I = RealPart1 * ImagePart2 + ImagePart1 * RealPart2;
        String s = null;
        if (I > 0) {
            s = R + "+" + I + "i";
        }
        if (I == 0) {
            s = R + "";
        }
        if (I < 0) {
            s = R + "-" + I * (-1) + "i";
        }
        return s;
    }

    public String ComplexDiv() {
        double R, I;
        double A = ImagePart2 * ImagePart2 + RealPart2 * RealPart2;
        R = (RealPart1 * RealPart2 + ImagePart1 * ImagePart2) / A;
        I = (ImagePart1 * RealPart2 - RealPart1 * ImagePart2) / A;
        String s = null;
        if (I > 0) {
            s = R + "+" + I + "i";
        }
        if (I == 0) {
            s = R + "";
        }
        if (I < 0) {
            s = R + "-" + I * (-1) + "i";
        }
        return s;
    }

    public String ComplexCompare() {
        String str2 = null, str3 = null, str4 = null;
        double part1 = Math.sqrt(RealPart1 * RealPart1 + ImagePart1 * ImagePart1);
        double part2 = Math.sqrt(RealPart2 * RealPart2 + ImagePart2 * ImagePart2);
        if (ImagePart1 > 0)
            str2 = RealPart1 + "+" + ImagePart1 + "i";
        else if (ImagePart1 == 0) {
            str2 = RealPart1 + "";
        } else if (ImagePart1 < 0) {
            str2 = RealPart1 + "-" + ImagePart1 * (-1) + "i";
        }
        if (ImagePart2 > 0) {
            str3 = RealPart2 + "+" + ImagePart2 + "i";
        }
        if (ImagePart2 == 0) {
            str3 = RealPart2 + "";
        }
        if (ImagePart2 < 0) {
            str3 = RealPart2 + "-" + ImagePart2 * (-1) + "i";
        }
        if (part1 > part2)
            str4 = str2 + ">" + str3;
        else if (part1 < part2)
            str4 = str2 + "<" + str3;
        else if (part1 == part2)
            str4 = str2 + "=" + str3;
        return str4;


    }

    public static void main(String[] args) {
        int a, num1, num2, num3, num4;
        double num5, num6, num7, num8;
        char char1, char2;
        RationalNumber r3 = null;
        newjisuan2 r4 = null;
        Scanner scan = new Scanner(System.in);
        do {
            System.out.println("进行有理数计算请输入1，进行复数计算请输入0");
            a = scan.nextInt();
            if (a == 1) {
                System.out.println("请输入第一个有理数（依次输入分子分母即可）");
                num1 = scan.nextInt();
                num2 = scan.nextInt();
                System.out.println("请输入第二个有理数（依次输入分子分母即可）");
                num3 = scan.nextInt();
                num4 = scan.nextInt();
                RationalNumber r1 = new RationalNumber(num1, num2);
                RationalNumber r2 = new RationalNumber(num3, num4);
                System.out.println("第一个有理数: " + r1);
                System.out.println("第二个有理数 " + r2);
                System.out.println("请输如要运算的符号：");
                char1 = scan.next().charAt(0);
                switch (char1) {
                    case '+':
                        r3 = r1.add(r2);
                        break;
                    case '-':
                        r3 = r1.subtract(r2);
                        break;
                    case '*':
                        r3 = r1.multiply(r2);
                        break;
                    case '/':
                        r3 = r1.divide(r2);
                        break;
                    default:
                        break;
                }
                System.out.println("r1" + char1 + "r2 = " + r3);

            } else if (a == 0) {
                String str1 = null;
                System.out.println("请输入第一个复数（依次输入实部虚部即可）");
                num5 = scan.nextDouble();
                num6 = scan.nextDouble();
                System.out.println("请输入第二个复数（依次输入实部虚部即可）");
                num7 = scan.nextDouble();
                num8 = scan.nextDouble();
                newjisuan2 r5 = new newjisuan2(num5, num6, num7, num8);
                System.out.println("请输如要运算的符号(+,-,*,/,=)：");
                char1 = scan.next().charAt(0);
                switch (char1) {
                    case '+':
                        str1 = r5.ComplexAdd();
                        break;
                    case '-':
                        str1 = r5.ComplexDiv();
                        break;
                    case '*':
                        str1 = r5.ComplexMulti();
                        break;
                    case '/':
                        str1 = r5.ComplexSub();
                    case '=':
                        str1 = r5.ComplexCompare();
                        break;

                    default:
                        break;
                }
                System.out.println(str1);

            }
            System.out.println("还要继续进行计算吗(y/n)?");
            char2 = scan.next().charAt(0);
        } while (char2 == 'y');
    }
}
