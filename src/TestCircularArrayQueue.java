import java.util.Scanner;

public class TestCircularArrayQueue {
    public static void main(String[] args) {
        CircularArrayQueue<Integer> circul1 = new CircularArrayQueue<Integer>();
        Scanner scan = new Scanner(System.in);
        final int DEFAULT_CAPACITY = 5;
        int i = 0;
        for (i = 0; i < DEFAULT_CAPACITY; i++) {
            System.out.println("Please enter 5 number.");
            int num1 = scan.nextInt();
            circul1.enqueue(num1);
        }

        System.out.println("Before expandCapacity: " + circul1.toString());
        System.out.println("First number: " + circul1.first());
        System.out.println("Count: " + circul1.size());
        if (circul1.isEmpty())
            System.out.println("Empty.");
        else
            System.out.println("Not Empty.");
        for (i = 0; i < DEFAULT_CAPACITY; i++) {
            System.out.println("Number out: " + circul1.dequeue());
        }
        System.out.println(circul1.toString());
        if (circul1.isEmpty())
            System.out.println("Empty.");
        else
            System.out.println("Not Empty.");
    }

}
