import java.io.*;

public class ComplexAdd {
    public static void main(String[] args) throws IOException {
        File file1 = new File("C:\\Users\\666\\IdeaProjects\\dky-zzz-java", "Complexbefore.txt");
        if (!file1.exists()) {
            file1.createNewFile();
        }
        File file2 = new File("C:\\Users\\666\\IdeaProjects\\dky-zzz-java", "HelloWorld4.txt");
        if (!file2.exists()) {
            file2.createNewFile();
        }//创建文件
        String info = null;
        Writer writer2 = new FileWriter(file1);//直接用writer写
        writer2.write("3.5+7.2i");//调用写方法
        writer2.flush();
        writer2.append("4.0+2.222i");//在写好的东西后面继续写
        writer2.flush();
        Reader reader2 = new FileReader(file1);//读取
        BufferedReader bufferedReader = new BufferedReader(reader2);

        System.out.println("写入并读取复数。");
        while ((info = bufferedReader.readLine()) != null) {
            System.out.println(info);
            int i, j;
            for (i = 0, j = 0; j < info.length(); j++) {
                if (!((info.charAt(j) >= '0' && info.charAt(j) <= '9') || info.charAt(j) == '.' || info.charAt(j) == '-'))//要求非空格形式，顶格
                    break;
            }
            double a = Double.parseDouble(info.substring(i, j));//转换为double类型
            j++;
            for (i = j; j < info.length(); j++) {
                if (!(info.charAt(j) >= '0' && info.charAt(j) <= '9' || info.charAt(j) == '.'))
                    break;
            }
            double b = Double.parseDouble(info.substring(i, j));
            if (info.charAt(i - 1) == '-') b = -b;//得到负值
            j++;
            for (i = j; j < info.length(); j++) {
                if (!(info.charAt(j) >= '0' && info.charAt(j) <= '9' || info.charAt(j) == '.'))
                    break;
            }
            double c = Double.parseDouble(info.substring(i, j));

            i = j++;
            j = info.length() - 1;
            double d = Double.parseDouble(info.substring(i, j));
            if (info.charAt(i - 1) == '-') d = -d;//这样就不能有空格
            Complex x = new Complex(a, b);
            Complex y = new Complex(c, d);
            Complex answer1 = x.ComplexAdd(y);
            String reply1 = answer1.toString();
            System.out.println("计算出结果。");
            System.out.println(reply1);
            System.out.println("将结果写入文件。");
            Writer writer3 = new FileWriter(file2);//直接用writer写
            writer3.write(reply1);//调用写方法
            writer3.flush();

        }
    }
}