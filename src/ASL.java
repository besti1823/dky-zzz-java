public class ASL {

    public boolean order(int[] arr, int target) {
        int i = 0;
        int a = target;
        while (arr[i] != target) {
            i++;
            if (i == arr.length)
                break;
        }
        return i == arr.length ? false : true;
    }

    public void sort(int shuzu[]) {
        int i, j, temp = 0;
        for (i = 0; i < shuzu.length - 1; i++) {

            for (j = i + 1; j < shuzu.length; j++) {
                if (shuzu[i] > shuzu[j]) {
                    temp = shuzu[i];
                    shuzu[i] = shuzu[j];
                    shuzu[j] = temp;
                }
            }
        }

    }

    public boolean binary(int[] arr, int min, int max, int mid, int target) {
        boolean found = false;
        mid = (min + max) / 2;
        int midd = mid;

        if (arr[midd] == target)
            found = true;
        else if (arr[midd] != target) {
            if (target < arr[midd]) {
                max = midd - 1;
                midd--;
                found = binary(arr, min, max, midd, target);
            } else if (target > arr[midd]) {
                min = midd + 1;
                midd++;
                found = binary(arr, min, max, midd, target);
            }
        }
        return found;
    }

    public int binaryshow(int[] arr, int min, int max, int mid, int target) {
        int found = 0;
        mid = (min + max) / 2;
        int midd = mid;

        if (arr[midd] == target)
            found = arr[midd];
        else if (arr[midd] != target) {
            if (target < arr[midd]) {

                max = midd - 1;
                midd--;
                found = binaryshow(arr, min, max, midd, target);
            } else if (target > arr[midd]) {
                min = midd + 1;
                midd++;
                found = binaryshow(arr, min, max, midd, target);
            }
        }
        return found;
    }


    public int[] hash(int[] arr) {
        int[] arr1 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        for (int i = 0; i < arr.length; i++) {
            if (arr1[arr[i] % 11] == 0)
                arr1[arr[i] % 11] = arr[i];
            else {
                for (int j = 1; j <= arr.length; j++)
                    if (arr1[(arr[i] + j) % 11] == 0) {
                        arr1[(arr[i] + j) % 11] = arr[i];
                        break;
                    }
            }
        }
        return arr1;
    }

    public int hashsearch(int[] result, int target) {
        int k = target % 11, i, re = 0;
        if (result[k] == target)
            re = result[k];
        else {
            for (i = 1; i <= result.length; i++) {
                if (result[(target + i) % 11] == target) {
                    re = (target + i) % 11;
                    break;
                }
            }
        }
        return re;
    }

    public static int[][] addhasgIP(int[] data) {
        int[][] result = new int[11][100];/*定义变量*/
        int n = 0;
        for (int i = 0; i < data.length; i++) {
            int m = data[i] % 11;
            while (result[m][n] != 0)
                n++;
            result[m][n] = data[i];
        }
        return result;/*返回值*/

    }

    public static int hashIP(int[][] data, int target) {
        int n = 0;/*定义变量*/
        int m = target % 11;
        while (data[m][n] != target && m < data.length - 1) {
            n++;
        }
        return data[m][n];/*返回值*/

    }


    public Linked[] linkedhash(Linked[] linked) {
        Linked[] arr1 = new Linked[12];
        int i;
        for (i = 0; i < 12; i++)
            arr1[i] = new Linked(0);
        for (i = 0; i < linked.length; i++) {
            if ((arr1[linked[i].getnum() % 11]).getnum() == 0)
                arr1[linked[i].getnum() % 11] = linked[i];
            else {
                arr1[linked[i].getnum() % 11].setNext(linked[i]);
            }
        }
        return arr1;
    }

    public int linkedsearch(Linked[] re1, int target) {
        int k = target % 11, i, re = 0;
        if (re1[k].getnum() == target)
            re = re1[k].getnum();
        else {
            Linked re2 = re1[k].getNext();
            re2 = new Linked(0);
            if (re2.getnum() == target)
                re = re2.getnum();
        }
        return re;
    }


    public String print(int[] arr) {
        String result = "";
        for (int i = 0; i < arr.length; i++)
            result += "" + arr[i] + " ";
        return result;
    }

}