import java.util.Scanner;
import java.text.NumberFormat;
public class work34
{
public static void main (String[] args)
{
final double TAXRATE = 0.06;
int quantity;
double subtotal, tax, totalcost, unitprice;
Scanner scan = new Scanner(System.in);
NumberFormat fmt1 = NumberFormat.getCurrencyInstance();
NumberFormat fmt2 = NumberFormat.getPercentInstance();
System.out.println("Entre the quantity: ");
quantity = scan.nextInt();

System.out.println("Entre the unit price: ");
unitprice = scan.nextDouble();

subtotal = quantity * unitprice;
tax = subtotal * TAXRATE;
totalcost = subtotal + tax;
System.out.println("Subtotal: "+fmt1.format(subtotal));
System.out.println("Tax: " + fmt1.format(tax)+ "  at  " +
fmt2.format(TAXRATE));
System.out.println("Total: " + fmt1.format(totalcost));
}
}

