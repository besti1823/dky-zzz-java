public class Card {
    enum Color {HONGTAO, FANGPIAN, HEITAO, MEIHUA}

    private int num1;
    private int num2;
    Color co;

    public Card() {
        flip();
    }

    public void flip() {
        num1 = (int) (1 + Math.random() * 13);
        num2 = (int) (1 + Math.random() * 4);
        switch (num2) {
            case 1:
                co = Color.HONGTAO;
                break;
            case 2:
                co = Color.FANGPIAN;
                break;
            case 3:
                co = Color.HEITAO;
                break;
            case 4:
                co = Color.MEIHUA;
                break;
            default:
                break;

        }
    }

    @Override
    public String toString() {
        return "Card{" +
                "Point: " + num1 +
                ", Color: " + co +
                '}';
    }
}
