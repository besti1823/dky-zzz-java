public class Staff {
    private Staffmerber[] staffList;

    public Staff() {
        staffList = new Staffmerber[6];
        staffList[0] = new Executive("A", "house", "555-0649", "123-4556", 22423.07);
        staffList[1] = new Employee("B", "home", "666-4556", "23-4557", 6423.07);
        staffList[2] = new Employee("T", "town", "777-4006", "23-4558", 4423.07);
        staffList[3] = new Volunteer("C", "school1", "888-7700");
        staffList[4] = new Volunteer("D", "school2", "999-4506");
        staffList[5] = new Hourly("E", "room2", "100-7556", "23-4559", 23);
        ((Executive) staffList[0]).awardBonus(50000);
        ((Hourly) staffList[5]).addHours(40);//强转调用两个类

    }

    public void payday() {
        double amount;
        for (int count = 0; count < staffList.length; count++) {
            System.out.println(staffList[count]);//toString
            amount = staffList[count].pay();//pay方法的调用是多态的
            if (amount == 0)
                System.out.println("THANK YOU VERY MUCH!!!!!!!!!!!!!!!WITHOUT PAY");
            else
                System.out.println("Paid: " + amount);
            System.out.println("---------------------------------------------------------------------");

        }
    }

    public void holidays() {
        for (int count = 0; count < staffList.length; count++) {
            System.out.println(staffList[count]);
            int num1 = staffList[count].holiday();
            System.out.println("CONGRADULATIONS!!YOU HAVE " + num1 + " DAYS HOLIDAY.");
            System.out.println("---------------------------------------------------------------------");
        }
    }
}
