import java.text.*;
import java.util.*;
public class PP35
{
public static void main (String[] main)
{
Scanner scan = new Scanner(System.in);
DecimalFormat fmt = new DecimalFormat("0.####");
final double pie = 3.1415926;
double num1,num2,num3;
System.out.println("请输入球的半径:");
num1 = scan.nextDouble();
num2 = 4*pie*Math.pow(num1,3)/3;
num3 = 4*pie*Math.pow(num1,2);
System.out.println("球的体积是: " + fmt.format(num2) +"     球的表面积是: "+
fmt.format(num3) );
}
}
