import java.util.Scanner;

public class work102 {
    public static void main(String[] args) throws StringTooLongException {
        String str1 = "";
        Scanner scan = new Scanner(System.in);
        StringTooLongException problem = new StringTooLongException("Warning!!!This string has too much characters!!!");
        while (!str1.equals("not")) {
            System.out.println("Please input a string");
            String str = scan.next();
            if (str.length() > 20)
                throw problem;
            System.out.println("Entre (not) to stop?");
            str1 = scan.next();
        }
    }
}
