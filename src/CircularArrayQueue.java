public class CircularArrayQueue<T> implements Quene<T> {
    public final int DEFAULT_CAPACITY = 5;
    private int front, rear, count;
    private T[] queue;

    public CircularArrayQueue() {
        front = rear = count = 0;
        queue = (T[]) (new Object[DEFAULT_CAPACITY]);

    }

    public void enqueue(T element) {
        if (count == queue.length)
            expandCapacity();
        queue[rear] = element;
        rear = (rear + 1) % queue.length;
        count++;

    }

    public T dequeue() throws EmptyCollectionException {
        T num1;
        if (front == rear && queue[front] == null)
            throw new EmptyCollectionException("Empty!!!");
        else {
            num1 = queue[front];
            queue[front] = null;
            front = (front + 1) % queue.length;
            count--;
        }
        return num1;
    }

    public void expandCapacity() {
        T[] larger = (T[]) (new Object[queue.length * 2]);
        for (int index = 0; index < count; index++)
            larger[index] = queue[(front + index) % queue.length];
        front = 0;
        rear = count;
        queue = larger;
    }

    public T first() throws EmptyCollectionException {
        if (front == rear && queue[front] == null) {
            throw new EmptyCollectionException("Empty!!!");
        } else
            return queue[front];
    }

    public boolean isEmpty() {
        if (count == 0)
            return true;
        else
            return false;
    }


    public int size() {
        return count;
    }

    public String toString() {
        String result = "[";
        for (int i = 0; i < queue.length; i++) {
            result += "   " + queue[i];
        }
        result += "]";
        return result;
    }
}
