import java.util.Scanner;
import java.util.Stack;

public class Stackwork3 {
    private Stack<Integer> stack;

    public Stackwork3() {
        stack = new Stack<Integer>();
    }

    private boolean isOperator(String token) {
        return (token.equals("+") || token.equals("-") || token.equals("*") || token.equals("/"));
    }

    private int evaluateSingleOperator(char operator, int op1, int op2) {
        int result = 0;

        switch (operator) {
            case '+':
                result = op1 + op2;
                break;
            case '-':
                result = op1 - op2;
                break;
            case '*':
                result = op1 * op2;
                break;
            case '/':
                result = op1 / op2;
                break;
        }

        return result;
    }

    public int calculate(String str1) {
        int op1, op2, result = 0;
        String token;
        Scanner tok = new Scanner(str1);
        while (tok.hasNext()) {
            token = tok.next();

            if (isOperator(token)) {
                op2 = (stack.pop()).intValue();
                op1 = (stack.pop()).intValue();
                result = evaluateSingleOperator(token.charAt(0), op1, op2);
                stack.push(result);
            } else {
                stack.push(Integer.parseInt(token));
            }
        }
        return result;
    }
}

