import java.util.Scanner;

public class BoxTest {
    public static void main(String[] args) {
        double a,b,c;
        boolean d;
        Box box1 = new Box(0.3,1,2,true);
        Box box2 = new Box(10,2.55,3.76,false);
        Box box3 = new Box(5.23423,2.11,0.4,true);
        box1.setHigh(3.14);
        box2.setFull(true);
        box3.setTall(100.321);
        System.out.println(box1);
        System.out.println(box2);
        System.out.println(box3);
    }
}
