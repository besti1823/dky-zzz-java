public class Tasktest {
    public static void main(String[] args) {
        Task[] work;
        work = new Task[2];

        work[0] = new Thing1(10, "Do java homework.");
        work[1] = new Thing2(10, "Do java test.");


        if (work[0].compareTo(work[1]) < 0) {
            System.out.println(work[0]);
            System.out.println(work[1]);
        } else if (work[0].compareTo(work[1]) > 0) {
            System.out.println(work[1]);
            System.out.println(work[0]);
        } else {
            System.out.println("Do at the same time.");
            System.out.println(work[1]);
            System.out.println(work[0]);
        }
    }
}
