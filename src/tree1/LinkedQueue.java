package tree1;

public class LinkedQueue<T> {
    private T number;
    private LinkedQueue next = null;
    private int count = 0;

    public LinkedQueue() {
        number = null;
        next = null;
    }

    public LinkedQueue(T number) {
        this.number = number;
        next = null;
    }

    public LinkedQueue first() {
        LinkedQueue a = this.next;
        return a;
    }

    public boolean isEmpty() {
        if (this.next != null)
            return true;
        else return false;
    }

    public void enqueue(T a) {
        LinkedQueue temp = this;
        while (temp.next != null) {
            temp = temp.next;
        }
        temp.next.number = a;
        count++;
    }

    public T dequeue() {
        LinkedQueue a = this.next;
        this.next = this.next.next;
        count--;
        return (T) a.number;
    }

    public int size() {
        return count;
    }

    public String toString() {
        LinkedQueue temp = this.next;
        for (int i = 1; i <= count; i++) {
            System.out.println("" + i + "" + temp.number);
        }
        return null;
    }
}
