package tree1;

import java.util.Scanner;

public class ZAnalyzer {
    private LinkedBinaryTree<String> tree;

    public ZAnalyzer() {
        String e1 = "你喜欢看电影还是听音乐?(Y/N)";
        String e2 = "你喜欢看恐怖片还是喜剧片？(Y/N)";
        String e3 = "你喜欢听流行音乐还是古典音乐?(Y/N)";
        String e4 = "你胆子挺大的！！";
        String e5 = "恭喜，你是一个幽默的人！";
        String e6 = "你蛮有品味的！";
        String e7 = "恭喜，你接受能力不错！";


        LinkedBinaryTree<String> n1, n2, n3, n4, n5, n6;


        n1 = new LinkedBinaryTree<String>(e4);
        n2 = new LinkedBinaryTree<String>(e5);
        n3 = new LinkedBinaryTree<String>(e2, n2, n1);

        n4 = new LinkedBinaryTree<String>(e6);
        n5 = new LinkedBinaryTree<String>(e7);
        n6 = new LinkedBinaryTree<String>(e3, n4, n5);


        tree = new LinkedBinaryTree<String>(e1, n6, n3);
    }

    public void Ask() {
        Scanner scan = new Scanner(System.in);
        LinkedBinaryTree<String> current = tree;

        System.out.println("让我们开始一个简单的测试：");
        while (current.size() > 1) {
            System.out.println(current.getRootElement());
            if (scan.nextLine().equalsIgnoreCase("N"))
                current = current.getLeft();
            else
                current = current.getRight();
        }

        System.out.println(current.getRootElement());
    }
}

