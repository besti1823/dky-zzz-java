package tree1;

public class BinarySrarchTreeTest {
    public static void main(String[] args) throws ElementNotFoundException {
        LinkedBinarySearchTree<Integer> tese = new LinkedBinarySearchTree<>(10);

        tese.add(5);
        tese.add(8);
        tese.add(34);
        tese.add(99);
        tese.add(2);

        System.out.println("遍历:");
        System.out.println(tese.inorder2());

        tese.remove(2);
        tese.remove(99);
        System.out.println(tese.inorder2());
    }
}
