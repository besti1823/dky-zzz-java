package tree1;

import tree2.EmptyCollectionException;

import java.util.ArrayList;
import java.util.Iterator;

public class LinkedBinaryTree<T> implements BinaryTree<T> {
    public BTNode<T> root;

    public LinkedBinaryTree() {
        root = null;
    }

    public LinkedBinaryTree(T element) {
        root = new BTNode<T>(element);
    }

    public LinkedBinaryTree(T element, LinkedBinaryTree<T> left, LinkedBinaryTree<T> right) {
        root = new BTNode<T>(element);
        root.setLeft(left.root);
        root.setRight(right.root);
    }

    public T getRootElement() {
        if (root == null)
            throw new EmptyCollectionException("Get root operation "
                    + "failed. The tree is empty.");

        return root.getElement();
    }

    public LinkedBinaryTree<T> getLeft() {
        if (root == null)
            throw new EmptyCollectionException("Get left operation "
                    + "failed. The tree is empty.");

        LinkedBinaryTree<T> result = new LinkedBinaryTree<T>();
        result.root = root.getLeft();

        return result;
    }

    public T find(T target) {
        BTNode<T> node = null;

        if (root != null)
            node = root.find(target);

        if (node == null)
            throw new ElementNotFoundException("Find operation failed. "
                    + "No such element in tree.");

        return node.getElement();
    }

    public int size() {
        int result = 0;

        if (root != null)
            result = root.count();

        return result;
    }

    public Iterator<T> inorder() {
        ArrayList<T> iter = new ArrayList<T>();

        if (root != null)
            root.inorder(iter);

        return (Iterator<T>) iter;
    }

    public Iterator<T> levelorder() {
        LinkedQueue<BTNode<T>> queue = new LinkedQueue<BTNode<T>>();
        ArrayList<T> iter = new ArrayList<T>();

        if (root != null) {
            queue.enqueue(root);
            while (!queue.isEmpty()) {
                BTNode<T> current = queue.dequeue();

                iter.add(current.getElement());

                if (current.getLeft() != null)
                    queue.enqueue(current.getLeft());
                if (current.getRight() != null)
                    queue.enqueue(current.getRight());
            }
        }

        return (Iterator<T>) iter;
    }

    public Iterator<T> iterator() {
        return inorder();
    }

    public LinkedBinaryTree<T> getRight() {
        if (root == null)
            throw new EmptyCollectionException("Get left operation "
                    + "failed. The tree is empty.");

        LinkedBinaryTree<T> result = new LinkedBinaryTree<T>();
        result.root = root.getRight();

        return result;
    }

    public boolean contains(T target) {
        BTNode a = root.find(target);
        if (a == null)
            return false;
        else return true;
    }

    public boolean isEmpty() {
        if (root.getLeft() == null && root.getRight() == null)
            return false;
        else return true;
    }

    @Override
    public String toString() {
        return "LinkedBinaryTree{" +
                "root=" + root +
                '}';
    }

    public Iterator<T> preorder() {
        ArrayList<T> iter = new ArrayList<T>();

        if (root != null)
            root.preorder(iter);

        return (Iterator<T>) iter;
    }

    public Iterator<T> postorder() {
        ArrayList<T> iter = new ArrayList<T>();

        if (root != null)
            root.postorder(iter);

        return (Iterator<T>) iter;
    }

    public void postOrderTraverse(BTNode<T> root) {
        if (root != null) {
            postOrderTraverse(root.left);
            postOrderTraverse(root.right);
            System.out.print(root.element + "->");
        }
    }

    public LinkedBinaryTree<T> prein(String pre, String in) {
        char roo = pre.charAt(0);
        int a = in.indexOf(roo);
        String b = String.valueOf(roo);
        String inleft = in.substring(0, a);
        String inright = in.substring(a + 1);
        String preleft = pre.substring(1, a + 1);
        String preright = pre.substring(a + 1);
        LinkedBinaryTree<T> Lefttree = new LinkedBinaryTree<T>();
        LinkedBinaryTree<T> Righttree = new LinkedBinaryTree<T>();
        if (inleft.length() > 1)
            Lefttree.prein(preleft, inleft);
        else if (inleft.length() == 1)
            Lefttree = new LinkedBinaryTree<T>((T) inleft);
        if (inright.length() > 1)
            Righttree.prein(preleft, inleft);
        else if (inright.length() == 1)
            Righttree = new LinkedBinaryTree<T>((T) inleft);
        LinkedBinaryTree<T> result = new LinkedBinaryTree<T>((T) b, Lefttree, Righttree);
        return result;
    }

    public ArrayList<T> preorder2() {
        ArrayList<T> iter = new ArrayList<T>();

        if (root != null)
            root.preorder(iter);

        return iter;
    }

    public ArrayList<T> inorder2() {
        ArrayList<T> iter = new ArrayList<T>();

        if (root != null)
            root.inorder(iter);

        return iter;
    }

}