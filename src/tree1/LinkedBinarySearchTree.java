package tree1;

import tree3.BinarySearchTree;

import java.util.Iterator;
import java.util.Stack;

public class LinkedBinarySearchTree<T extends Comparable<T>>
        extends LinkedBinaryTree<T> implements BinarySearchTree<T> {
    public LinkedBinarySearchTree() {
        super();
    }

    public LinkedBinarySearchTree(T element) {
        root = new BSTNode<T>(element);
    }

    public void add(T item) {
        if (root == null)
            root = new BSTNode<T>(item);
        else
            ((BSTNode) root).add(item);
    }

    public T remove(T target) {
        BSTNode<T> node = null;

        if (root != null)
            node = ((BSTNode) root).find(target);

        if (node == null)
            throw new ElementNotFoundException("Remove operation failed. "
                    + "No such element in tree.");

        root = ((BSTNode) root).remove(target);

        return node.getElement();
    }

    public T findMax() {

        Iterator<T> iterator = this.preorder2().iterator();
        Stack<T> stack = new Stack<>();
        while (iterator.hasNext()) {
            stack.push(iterator.next());
        }

        T max, temp2;
        max = stack.pop();
        while (!stack.isEmpty()) {
            temp2 = stack.pop();
            if (temp2.compareTo(max) > 0) {
                max = temp2;
            }
        }
        return max;
    }

    public T findMin() {

        Iterator<T> iterator = this.preorder2().iterator();
        Stack<T> stack = new Stack<>();
        while (iterator.hasNext()) {
            stack.push(iterator.next());
        }

        T min, temp2;
        min = stack.pop();
        while (!stack.isEmpty()) {
            temp2 = stack.pop();
            if (temp2.compareTo(min) < 0) {
                min = temp2;
            }
        }
        return min;
    }
}
