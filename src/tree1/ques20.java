package tree1;

import java.util.Scanner;

public class ques20 {
    private LinkedBinaryTree<String> tree;

    public ques20() {
        String e1 = "大学中所学的数学是否需要积分?(Y/N)";
        String e2 = "计算难度大还是概念难度大？(Y/N)";
        String e3 = "有无大量矩阵运算(Y/N)";
        String e4 = "高等数学";
        String e5 = "概率论与数理统计";
        String e6 = "离散数学";
        String e7 = "线性代数";


        LinkedBinaryTree<String> n1, n2, n3, n4, n5, n6;


        n1 = new LinkedBinaryTree<String>(e4);
        n2 = new LinkedBinaryTree<String>(e5);
        n3 = new LinkedBinaryTree<String>(e2, n2, n1);

        n4 = new LinkedBinaryTree<String>(e6);
        n5 = new LinkedBinaryTree<String>(e7);
        n6 = new LinkedBinaryTree<String>(e3, n4, n5);


        tree = new LinkedBinaryTree<String>(e1, n6, n3);

    }

    public void test() {
        Scanner scan = new Scanner(System.in);
        LinkedBinaryTree<String> current = tree;

        System.out.println("Please have a 20 questions test.");
        while (current.size() > 1) {
            System.out.println(current.getRootElement());
            if (scan.nextLine().equalsIgnoreCase("N"))
                current = current.getLeft();
            else
                current = current.getRight();
        }

        System.out.println(current.getRootElement());
    }
}
