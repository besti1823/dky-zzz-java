public class Complex {

    double RealPart ;
    double ImagePart ;


    public Complex(double R, double I) {
        this.RealPart = R;
        this.ImagePart = I;

    }

    public  double getRealPart() {
        return RealPart;
    }

    public  double getImagePart() {
        return ImagePart;
    }

    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        else
            return false;
    }

    public String toString() {
        String s = null;
        if (ImagePart > 0) {
            s = RealPart + "+" + ImagePart + "i";
        }
        if (ImagePart == 0) {
            s = RealPart + "";
        }
        if (ImagePart < 0) {
            s = RealPart + "-" + ImagePart * (-1) + "i";
        }
        return s;
    }


    public Complex ComplexAdd(Complex a) {
        return new Complex(RealPart + a.RealPart, ImagePart + a.ImagePart);
    }

    public Complex ComplexSub(Complex a) {
        return new Complex(RealPart - a.RealPart, ImagePart - a.ImagePart);
    }

    public Complex ComplexMulti(Complex a) {

        return new Complex(RealPart * a.RealPart - ImagePart * a.ImagePart, RealPart * a.ImagePart + ImagePart * a.RealPart);

    }

    public Complex ComplexDiv(Complex a) {

        return new Complex((RealPart * a.ImagePart + ImagePart * a.RealPart) / (a.ImagePart * a.ImagePart + a.RealPart * a.RealPart),
                (ImagePart * a.ImagePart + RealPart * a.RealPart) / (a.ImagePart * a.ImagePart + a.RealPart * a.RealPart));

    }



}


