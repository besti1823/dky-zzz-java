import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class fuwuqi3 {
    public static void main(String[] args) throws IOException {
        //1.建立一个服务器Socket(ServerSocket)绑定指定端口
        ServerSocket serverSocket = new ServerSocket(8600);
        //2.使用accept()方法阻止等待监听，获得新连接
        Socket socket = serverSocket.accept();
        //3.获得输入流
        InputStream inputStream = socket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        //获得输出流
        OutputStream outputStream = socket.getOutputStream();
        PrintWriter printWriter = new PrintWriter(outputStream);
        //4.读取用户输入信息
        String info = null;
        System.out.println("Server already......");
        info = bufferedReader.readLine();
        System.out.println("server: " + info);


        //分解数据
        int i, j;
        char char1;
        for (i = 0, j = 0; j < info.length(); j++) {
            if (!(info.charAt(j) >= '0' && info.charAt(j) <= '9'))//要求非空格形式，顶格
                break;
        }
        int a = Integer.parseInt(info.substring(i, j));//转换为double类型
        j++;
        for (i = j; j < info.length(); j++) {
            if (!(info.charAt(j) >= '0' && info.charAt(j) <= '9'))
                break;
        }
        int b = Integer.parseInt(info.substring(i, j));
        if (info.charAt(i - 1) == '-') b = -b;//得到负值

        //找到运算符

        char1 = info.charAt(j);//保存运算符
        j++;
        for (i = j; j < info.length(); j++) {
            if (!(info.charAt(j) >= '0' && info.charAt(j) <= '9'))
                break;
        }
        int c = Integer.parseInt(info.substring(i, j));

        i = j + 1;
        j = info.length() - 1;
        int d = Integer.parseInt(info.substring(i, j));
        if (info.charAt(i - 1) == '-') d = -d;//这样就不能有空格

        RationalNumber x = new RationalNumber(a, b);
        RationalNumber y = new RationalNumber(c, d);
        //计算并给客户一个响应
        switch (char1) {
            case '+':
                RationalNumber answer1 = x.add(y);
                String reply1 = answer1.toString();
                printWriter.write(reply1);
                printWriter.flush();
                break;
            case '-':
                RationalNumber answer2 = x.subtract(y);
                String reply2 = answer2.toString();
                printWriter.write(reply2);
                printWriter.flush();
                break;
            case '*':
                RationalNumber answer3 = x.multiply(y);
                String reply3 = answer3.toString();
                printWriter.write(reply3);
                printWriter.flush();
                break;
            case '/':
                RationalNumber answer4 = x.divide(y);
                String reply4 = answer4.toString();
                printWriter.write(reply4);
                printWriter.flush();
                break;
            default:
                String replyf = "ERROR!NOT A OPTION";
                printWriter.write(replyf);
                printWriter.flush();
        }

        //5.关闭资源
        printWriter.close();
        outputStream.close();
        bufferedReader.close();
        inputStream.close();
        socket.close();
        serverSocket.close();
    }

}
