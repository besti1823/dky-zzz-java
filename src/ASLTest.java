import java.util.Scanner;

public class ASLTest {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        ASL asl = new ASL();
        int target = 0, i, j = 0;

        int[] shuzu = {19, 14, 23, 1, 68, 20, 84, 27, 55, 11, 10, 79};

        asl.sort(shuzu);
        System.out.println("排序后 : " + asl.print(shuzu));

        System.out.println("顺序查找，输入要查找的数:");
        target = scan.nextInt();
        System.out.println("是否找到 :" + asl.order(shuzu, target));

        System.out.println("折半查找，输入要查找的数:");
        target = scan.nextInt();
        System.out.println("是否找到 :" + asl.binary(shuzu, 0, 11, 5, target));
        System.out.println("找到的数是:" + asl.binaryshow(shuzu, 0, 11, 5, target));

        System.out.println("线性探查法查找，输入要查找的数:");
        target = scan.nextInt();
        int result1[] = asl.hash(shuzu);
        int result2 = asl.hashsearch(result1, target);
        if (result2 == 0)
            System.out.println("查找失败！数组中无此数！");
        else
            System.out.println("查找成功！查找到的数是:" + result1[result2]);

        System.out.println("链地址法查找，输入要查找的数:");
        target = scan.nextInt();
        Linked[] linked = new Linked[12];
        for (i = 0; i < 12; i++)
            linked[i] = new Linked(shuzu[i]);
        Linked[] re1 = asl.linkedhash(linked);
        int ree = asl.linkedsearch(re1, target);
        if (ree == 0)
            System.out.println("查找失败！数组中无此数！");

        else

            System.out.println("查找成功！查找到的数是:" + ree);


        System.out.println("请输入你想要查找的数（哈希线性查找）");

        int a = scan.nextInt();

        Compareable target1;
        target1 = new Compareable(a);

        int[] b = new int[12];
        BinaryTree e = new BinaryTree();
        e.s(shuzu);
        Compareable tree = e.get();
        boolean k = treesearch.erchashu(tree, target1);
        System.out.println(k);
    }
}