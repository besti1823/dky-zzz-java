public abstract class HospitalEmployee {
    public String Ask() {
        return "Hello,what can I do for you?";
    }

    public abstract String Do();

    public abstract String tostring();
}
