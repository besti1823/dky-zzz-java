public class Book2 {
    protected int pages;

    public Book2(int numPages) {
        this.pages = numPages;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int numPages) {
        this.pages = numPages;
    }
}
