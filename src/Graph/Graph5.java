package Graph;

import org.w3c.dom.ls.LSOutput;

import java.util.Scanner;

public class Graph5 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("请输入图顶点的个数：");
        int a, b, i, j;
        b = scan.nextInt();
        System.out.println("请输入图的顶点矩阵：");
        int dingdian[] = new int[b];
        for (i = 0; i < b; i++) {
            dingdian[i] = scan.nextInt();
        }
        System.out.println("请输入1为有项图，0为无向图：");
        a = scan.nextInt();
        if (a == 1) System.out.println("请输入有向图的邻接矩阵！");
        else System.out.println("请输入无向图的邻接矩阵！");
        int weight[][] = new int[b][b];
        for (i = 0; i < b; i++) {
            for (j = 0; j < b; j++) {
                weight[i][j] = scan.nextInt();
            }
        }
        System.out.println("输出图：");
        System.out.println("邻接数组：");
        for (i = 0; i < b; i++) {
            System.out.print(dingdian[i]);
        }
        System.out.println();
        System.out.println("邻接矩阵：");
        for (i = 0; i < b; i++) {
            for (j = 0; j < b; j++) {
                System.out.print(weight[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();

    }


}
