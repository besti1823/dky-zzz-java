package Graph;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;

public class Graph2 {

    private Map<String, List<String>> graphMap = new HashMap<String, List<String>>();

    public void initGraph() {
        /**
         * 初始化图结构，将每个图节点为key,它的相邻节点以list为value的形式存放到map中
         */
        graphMap.put("1", Arrays.asList("2", "3"));
        graphMap.put("2", Arrays.asList("1", "4", "5"));
        graphMap.put("3", Arrays.asList("1", "6", "7"));
        graphMap.put("4", Arrays.asList("2", "8"));
        graphMap.put("5", Arrays.asList("2", "8"));
        graphMap.put("6", Arrays.asList("3", "8", "9"));
        graphMap.put("7", Arrays.asList("3", "9"));
        graphMap.put("8", Arrays.asList("4", "5", "6"));
        graphMap.put("9", Arrays.asList("6", "7"));
    }

    //用来记录节点是否被访问过
    private Map<String, Boolean> status = new HashMap<String, Boolean>();
    //建一个队列用来进行宽度优先遍历
    private Queue<String> queue = new LinkedList<String>();

    public void BreadthFS(String startPoint) {
        //将图的起始点入队
        queue.offer(startPoint);
        status.put(startPoint, true);
        while (!queue.isEmpty()) {
            //将队首的元素出队
            String tempPoint = queue.poll();
            System.out.print(tempPoint + "-");
            //遍历之后的节点，将其状态改为false
            status.put(tempPoint, false);
            //遍历该节点邻接的节点
            for (String point : graphMap.get(tempPoint)) {
                //如果该节点被访问过，则不入队
                //getOrDefault:当集合中不存在该key，或者该key的值为null时则默认值为true
                if (status.getOrDefault(point, true)) {
                    //如果队列中已经存在了该节点，则不再次入队
                    if (!queue.contains(point)) {
                        queue.offer(point);
                    }
                }
            }
        }
    }

    public void depthFS(String startPoint) {
        //建立一个栈用来进行深度优先遍历
        Stack<String> stack = new Stack<String>();
        //用来记录栈中元素的状态
        Map<String, Boolean> status = new HashMap<String, Boolean>();
        //将起始的点入栈
        stack.push(startPoint);
        while (!stack.isEmpty()) {
            //-----------显示栈内的数据情况----------------
            System.out.print("\t");
            for (String string : stack) {
                System.out.print(string);
                if (!string.equals(stack.lastElement())) {
                    System.out.print("->");
                }
            }
            System.out.println();
            //---------------------------
            String tempPoint = stack.pop();//出栈

            //出栈后的元素标记为已遍历(false)
            status.put(tempPoint, false);
            //打印出栈顺序
            System.out.print(tempPoint);
            for (String point : graphMap.get(tempPoint)) {
                //getOrDefault:当集合中不存在该key，或者该key的值为null时则默认值为true
                if (status.getOrDefault(point, true)) {
                    //如果包含这个栈中没有这个元素-->入栈
                    //有这个元素，先删除栈中的该元素，再入栈。
                    if (!stack.contains(point)) {
                        stack.push(point);
                    } else {
                        stack.remove(point);
                        stack.push(point);
                    }
                }
            }
        }
    }

    //用递归来实现无向图的深度优先遍历
    public void depthFSwithRecusive(String StartPoint) {
        if (status.getOrDefault(StartPoint, true)) {
            System.out.print(StartPoint + "-");
            status.put(StartPoint, false);
        }
        for (String point : graphMap.get(StartPoint)) {
            if (status.getOrDefault(point, true)) {
                depthFSwithRecusive(point);
            }
        }
    }
}