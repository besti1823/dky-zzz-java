public class Task implements Priority {
    int priority;

    String str1;

    public Task(int priority, String str1) {
        this.priority = priority;
        this.str1 = str1;
    }

    public void setPriority(int a) {
        this.priority = a;
    }

    public int getPriority() {
        return priority;
    }

    @Override
    public String toString() {
        return "Task: " +
                "Priority:" + priority +
                ", Work:" + str1;
    }

    public int compareTo(Task a) {
        if (this.priority > a.priority)
            return 1;
        else if (this.priority < a.priority)
            return -1;
        else
            return 0;

    }
}
