public class Box {
    private double wide;
    private double high;
    private double tall;
    private boolean full = false;

    public Box(double wide, double high, double tall, boolean full) {
        this.wide = wide;
        this.high = high;
        this.tall = tall;
        this.full = full;
    }

    public double getWide() {
        return wide;
    }

    public void setWide(double wide) {
        this.wide = wide;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    public double getTall() {
        return tall;
    }

    public void setTall(double tall) {
        this.tall = tall;
    }

    public boolean isFull() {
        return full;
    }

    public void setFull(boolean full) {
        this.full = full;
    }

    @Override
    public String toString() {
        return "Box{" +
                "wide=" + wide +
                ", high=" + high +
                ", tall=" + tall +
                ", full=" + full +
                '}';
    }
}
