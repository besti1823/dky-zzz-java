public class Coin2 {
    enum Con{HEADS,TAILS}
        private final int head =0;
        private int face;

        public Coin2() {

            flip();
        }

        public void flip()

        {
            face = (int) (Math.random()*2);
        }

        public Con isHeads ()
        {
            if (face == head )
                return Con.HEADS;
            else
                return Con.TAILS;
        }

        public String toString()
        {
            return (face == head) ? "Heads" : "Tails";
        }

    }


