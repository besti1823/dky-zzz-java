public class Hourly extends Employee {
    private int hourWorked;

    public Hourly(String name, String address, String phone, String socialSecurityNumber, double payRate) {
        super(name, address, phone, socialSecurityNumber, payRate);
        this.hourWorked = 0;
    }

    public void addHours(int moreHours) {
        hourWorked += moreHours;
    }

    public double pay() {
        double payment = payRate * hourWorked;
        hourWorked = 0;
        return payment;
    }

    public String toString() {
        String result = super.toString();
        result += "\nCurrent hours: " + hourWorked;
        //count--;
        return result;
    }

    public int holiday() {
        return 3;
    }
}
