
public class TreeBinarytest {
    public static void main(String args[]) {
        System.out.println("输入为：AB#CD###E#F##");

        TreeBinary.ch1 = new char[20];

        TreeBinary.ch1 = "AB#CD###E#F##".toCharArray();
        TreeBinary.root = TreeBinary.CreateTree();

        System.out.println("层次遍历：");
        TreeBinary.levOrder(TreeBinary.root);
        System.out.println();
        System.out.println("\n先序遍历：");
        TreeBinary.preOrder(TreeBinary.root);
        System.out.println("\n中序遍历：");
        TreeBinary.inOrder(TreeBinary.root);
        System.out.println("\n后序遍历：");
        TreeBinary.postOrder(TreeBinary.root);
        System.out.println("\n非递归先序：");
        TreeBinary.preOrderTraverse(TreeBinary.root);
        System.out.println("\n非递归中序：");
        TreeBinary.inOrderTraverse(TreeBinary.root);//非递归中序
        System.out.println();


    }
}
