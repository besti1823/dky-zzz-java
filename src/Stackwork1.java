import java.util.Scanner;
import java.util.Stack;

public class Stackwork1 {
    private Stack<Integer> stack;

    public Stackwork1() {
        stack = new Stack<Integer>();
    }

    public int calculate(String expr) {
        int op1, op2, result = 0;
        String token;
        Scanner parser = new Scanner(expr);

        while (parser.hasNext()) {
            token = parser.next();

            if (isOperator(token)) {
                op2 = (stack.pop()).intValue();
                op1 = (stack.pop()).intValue();
                result = evaluateSingleOperator(token.charAt(0), op1, op2);
                stack.push(new Integer(result));
            } else {
                stack.push(new Integer(Integer.parseInt(token)));
            }
        }
        return result;
    }

    private boolean isOperator(String token) {
        return (token.equals("+") || token.equals("-") || token.equals("*") || token.equals("/"));
    }

    private int evaluateSingleOperator(char operator, int op1, int op2) {
        int result = 0;

        switch (operator) {
            case '+':
                result = op1 + op2;
                break;
            case '-':
                result = op1 - op2;
                break;
            case '*':
                result = op1 * op2;
                break;
            case '/':
                result = op1 / op2;
                break;
        }

        return result;
    }
}