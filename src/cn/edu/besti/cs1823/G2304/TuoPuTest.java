package cn.edu.besti.cs1823.G2304;

public class TuoPuTest {
    public static void main(String[] args) {
        TuoPu topological = new TuoPu();
        System.out.println("图一：");
        topological.createGraph1();
        boolean a = topological.topologicalSort();
        if (a) {
            System.out.println("\n没有环");
        } else {
            System.out.println("\n存在环");
        }

        TuoPu topological2 = new TuoPu();
        topological2.createGraph2();
        System.out.println("图二：");
        boolean success2 = topological2.topologicalSort();
        if (success2) {
            System.out.println("\n没有环");
        } else {
            System.out.println("\n存在环");
        }
    }

}
