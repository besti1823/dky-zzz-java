package cn.edu.besti.cs1823.G2304;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

class EdgeNode {
    private int adjvex;
    private int weight;
    private EdgeNode next;

    public EdgeNode(int adjvex, int weight, EdgeNode next) {
        super();
        this.adjvex = adjvex;
        this.weight = weight;
        this.next = next;
    }

    public int getAdjvex() {
        return adjvex;
    }

    public void setAdjvex(int adjvex) {
        this.adjvex = adjvex;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public EdgeNode getNext() {
        return next;
    }

    public void setNext(EdgeNode next) {
        this.next = next;
    }
}

class VertexNode {
    private int in;
    private int data;
    private EdgeNode firstEdge;

    public VertexNode(int in, int data, EdgeNode firstEdge) {
        super();
        this.in = in;
        this.data = data;
        this.firstEdge = firstEdge;
    }

    public int getIn() {
        return in;
    }

    public void setIn(int in) {
        this.in = in;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public EdgeNode getFirstEdge() {
        return firstEdge;
    }

    public void setFirstEdge(EdgeNode firstEdge) {
        this.firstEdge = firstEdge;
    }
}

public class TuoPu {
    private List<VertexNode> vexList;

    public void createGraph1() {
        VertexNode v0 = new VertexNode(0, 1, null);
        EdgeNode v0e0 = new EdgeNode(1, 0, null);
        EdgeNode v0e1 = new EdgeNode(2, 0, null);

        v0.setFirstEdge(v0e0);
        v0e0.setNext(v0e1);

        VertexNode v1 = new VertexNode(0, 1, null);
        EdgeNode v1e0 = new EdgeNode(3, 0, null);
        EdgeNode v1e1 = new EdgeNode(4, 0, null);

        v1.setFirstEdge(v1e0);
        v1e0.setNext(v1e1);

        VertexNode v2 = new VertexNode(1, 2, null);
        EdgeNode v2e0 = new EdgeNode(3, 0, null);
        EdgeNode v2e1 = new EdgeNode(5, 0, null);

        v2.setFirstEdge(v2e0);
        v2e0.setNext(v2e1);


        VertexNode v3 = new VertexNode(2, 3, null);
        EdgeNode v3e0 = new EdgeNode(5, 0, null);

        v3.setFirstEdge(v3e0);


        VertexNode v4 = new VertexNode(1, 4, null);
        EdgeNode v4e0 = new EdgeNode(5, 0, null);
        v4.setFirstEdge(v4e0);


        VertexNode v5 = new VertexNode(3, 5, null);

        vexList = new ArrayList<>();
        vexList.add(v0);
        vexList.add(v1);
        vexList.add(v2);
        vexList.add(v3);
        vexList.add(v4);
        vexList.add(v5);
    }

    public void createGraph2() {
        VertexNode v0 = new VertexNode(0, 0, null);
        EdgeNode v0e0 = new EdgeNode(1, 0, null);
        EdgeNode v0e1 = new EdgeNode(2, 0, null);
        v0.setFirstEdge(v0e0);
        v0e0.setNext(v0e1);

        VertexNode v1 = new VertexNode(1, 1, null);
        EdgeNode v1e0 = new EdgeNode(5, 0, null);
        v1.setFirstEdge(v1e0);

        VertexNode v2 = new VertexNode(1, 2, null);
        EdgeNode v2e0 = new EdgeNode(3, 0, null);
        v2.setFirstEdge(v2e0);


        VertexNode v3 = new VertexNode(2, 3, null);
        EdgeNode v3e0 = new EdgeNode(4, 0, null);
        v3.setFirstEdge(v3e0);


        VertexNode v4 = new VertexNode(1, 4, null);
        EdgeNode v4e0 = new EdgeNode(5, 0, null);
        EdgeNode v4e1 = new EdgeNode(6, 0, null);
        v4.setFirstEdge(v4e0);
        v4e0.setNext(v4e1);

        VertexNode v6 = new VertexNode(1, 6, null);
        EdgeNode v6e0 = new EdgeNode(7, 0, null);
        v6.setFirstEdge(v6e0);

        VertexNode v7 = new VertexNode(1, 7, null);
        EdgeNode v7e0 = new EdgeNode(3, 0, null);
        v7.setFirstEdge(v7e0);

        VertexNode v5 = new VertexNode(2, 5, null);


        vexList = new ArrayList<>();
        vexList.add(v0);
        vexList.add(v1);
        vexList.add(v2);
        vexList.add(v3);
        vexList.add(v4);
        vexList.add(v5);
        vexList.add(v6);
        vexList.add(v7);

    }


    public boolean topologicalSort() {
        int count = 0;//统计输出数
        Stack<Integer> stack = new Stack<>();//建栈存储入度为0的顶点
        for (int i = 0; i < vexList.size(); i++) {
            vexList.get(i).setIn(0);
        }
        for (int i = 0; i < vexList.size(); i++) {
            EdgeNode edge = vexList.get(i).getFirstEdge();
            while (edge != null) {
                VertexNode vex = vexList.get(edge.getAdjvex());
                vex.setIn(vex.getIn() + 1);
                edge = edge.getNext();
            }
        }
        //将入度为0 的顶点入栈
        for (int i = 0; i < vexList.size(); i++) {
            if (vexList.get(i).getIn() == 0) {
                stack.push(i);
            }
        }
        while (!stack.isEmpty()) {
            //栈顶 出栈
            int vexIndex = stack.pop();
            System.out.print(vexIndex + 1 + "  ");
            count++;
            //从顶点表结点中取出第一个边表结点
            EdgeNode edge = vexList.get(vexIndex).getFirstEdge();
            while (edge != null) {
                int adjvex = edge.getAdjvex();
                VertexNode vex = vexList.get(adjvex);
                //将此 顶点的入度减一
                vex.setIn(vex.getIn() - 1);
                //此顶点的入度为零则入栈，以便于下次循环输出
                if (vex.getIn() == 0) {
                    stack.push(adjvex);
                }
                edge = edge.getNext();
            }
        }
        if (count != vexList.size())
            return false;
        else
            return true;
    }

}

