package cn.edu.besti.cs1823.G2304;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;


public class Graph {
    //节点个数
    private static int number = 8;
    //创立访问标志数组的布尔型数组
    private boolean[] flag;

    //创立要遍历节点的数组
    private int[] num = {1, 2, 3, 4, 5, 6, 7, 8};
    //创立这几个数字的邻接矩阵
    private int[][] edges = {
            /*{0, 1, 1, 0, 0, 0, 0, 0},
            {1, 0, 0, 1, 1, 0, 0, 0},
            {1, 0, 0, 0, 0, 1, 1, 0},
            {0, 1, 0, 0, 0, 0, 0, 1},
            {0, 1, 0, 0, 0, 0, 0, 1},
            {0, 0, 1, 0, 0, 0, 1, 0},
            {0, 0, 1, 0, 0, 1, 0, 0},
            {0, 0, 0, 1, 1, 0, 0, 0},*/
            {0, 1, 1, 0, 0, 0, 0, 0},
            {0, 0, 0, 1, 1, 0, 0, 0},
            {0, 0, 0, 0, 0, 1, 1, 0},
            {0, 0, 0, 0, 0, 0, 0, 1},
            {0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 1, 0},
            {0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 1, 0, 0, 0},

    };

    void DFSTraverse() {
        //设置一个和数字个数同等大小的布尔数组
        flag = new boolean[number];
        //从顶点开始，实现深度遍历
        for (int i = 0; i < number; i++) {
            if (flag[i] == false) {
                // 如果当前顶点没有被访问，进入DFS
                DFS(i);
            }
        }
    }

    //完成一次遍历，直到后面无连接节点
    void DFS(int i) {
        // 标记第num[i]个节点被访问
        flag[i] = true;
        //将该节点打印
        System.out.print(num[i] + " ");
        //寻找与num[i]节点相连的下一个访问节点
        for (int j = 0; j < number; j++) {
            //从标志数组第0位开始顺序查找，如果这一点未被访问，且与第num[i]个节点相连
            if (flag[j] == false && edges[i][j] == 1) {
                //递归
                DFS(j);
            }
        }
    }

    void DFS_Map() {
        flag = new boolean[number];
        Stack<Integer> stack = new Stack<Integer>();
        for (int i = 0; i < number; i++) {
            if (flag[i] == false) {
                flag[i] = true;
                System.out.print(num[i] + " ");
                stack.push(i);
            }
            while (!stack.isEmpty()) {
                int k = stack.pop();
                for (int j = 0; j < number; j++) {
                    if (edges[k][j] == 1 && flag[j] == false) {
                        flag[j] = true;
                        System.out.print(num[j] + " ");
                        stack.push(j);
                        break;
                    }
                }

            }
        }
    }

    void BFS_Map() {
        flag = new boolean[number];
        Queue<Integer> queue = new LinkedList<Integer>();
        for (int i = 0; i < number; i++) {
            if (flag[i] == false) {
                flag[i] = true;
                System.out.print(num[i] + " ");
                queue.add(i);
                while (!queue.isEmpty()) {
                    int k = queue.poll();
                    for (int j = 0; j < number; j++) {
                        if (edges[k][j] == 1 && flag[j] == false) {
                            flag[j] = true;
                            System.out.print(num[j] + " ");
                            queue.add(j);
                        }
                    }
                }
            }
        }
    }


    //测试类
    public static void main(String[] args) {
        System.out.println("有向图遍历！");
        Graph graph = new Graph();
        System.out.println("DFS递归:");
        graph.DFSTraverse();
        System.out.println();

        System.out.println("DFS非递归:");
        graph.DFS_Map();

        System.out.println();
        System.out.println("BFS非递归:");
        graph.BFS_Map();

    }
} 

