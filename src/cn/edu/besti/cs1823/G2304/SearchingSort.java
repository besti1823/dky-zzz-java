package cn.edu.besti.cs1823.G2304;

public class SearchingSort {
    public static Comparable linearSearch(Comparable[] data, Comparable target) {
        Comparable result = null;
        int index = 0;
        while (result == null && index < data.length) {
            if (data[index].compareTo(target) == 0)
                result = data[index];
            index++;
        }
        return result;

    }

    public static void selectSort(Comparable[] data) {
        int min;
        for (int index = 0; index < data.length - 1; index++) {
            min = index;
            for (int scan = index + 1; scan < data.length; scan++) {
                if (data[scan].compareTo(data[min]) < 0)
                    min = scan;
                swap(data, min, index);
            }
        }
    }

    private static void swap(Comparable[] data, int index1, int index2) {
        Comparable temp = data[index1];
        data[index1] = data[index2];
        data[index2] = temp;
    }
}
