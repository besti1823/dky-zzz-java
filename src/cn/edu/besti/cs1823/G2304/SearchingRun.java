package cn.edu.besti.cs1823.G2304;

public class SearchingRun {
    public static void main(String[] args) {
        Comparable[] a = new Comparable[12];
        a[0] = 889;
        a[1] = 14;
        a[2] = 23;
        a[3] = 4;
        a[4] = 68;
        a[5] = 20;
        a[6] = 84;
        a[7] = 27;
        a[8] = 55;
        a[9] = 11;
        a[10] = 124;
        a[11] = 79;

        System.out.print("排序前：");
        for (int i = 0; i < 12; i++) System.out.print(a[i] + " ");
        System.out.println();

        SearchingSort.selectSort(a);

        System.out.print("线性排序后：");
        for (int i = 0; i < 12; i++)
            System.out.print(a[i] + " ");
        System.out.println();

        System.out.print("倒序排序后：");
        for (int i = 11; i >= 0; i--)
            System.out.print(a[i] + " ");
        System.out.println();

        System.out.println("(正常)找到：" + SearchingSort.linearSearch(a, 20));
        System.out.println("(异常)找到：" + SearchingSort.linearSearch(a, 444));
        System.out.println("(边界)找到：" + SearchingSort.linearSearch(a, 4));
    }
}
