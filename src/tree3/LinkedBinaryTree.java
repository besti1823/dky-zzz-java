package tree3;

import java.util.*;

import tree2.EmptyCollectionException;
import tree1.ElementNotFoundException;
import tree1.NonComparableElementException;

public class LinkedBinaryTree<T> implements BinaryTreeADT<T>, Iterable<T> {
    protected BinaryTreeNode<T> root;
    protected int modCount;

    public LinkedBinaryTree() {
        root = null;
    }

    public LinkedBinaryTree(T element) {
        root = new BinaryTreeNode<T>(element);
    }

    public LinkedBinaryTree(T element, LinkedBinaryTree<T> left,
                            LinkedBinaryTree<T> right) {
        root = new BinaryTreeNode<T>(element);
        root.setLeft(left.root);
        root.setRight(right.root);
    }

    @Override
    public T getRootElement() throws EmptyCollectionException {
        if (root == null) {
            throw new EmptyCollectionException("BinaryTree");
        }
        return root.getElement();
    }

    protected BinaryTreeNode<T> getRootNode() throws EmptyCollectionException {
        return root;
    }

    public LinkedBinaryTree<T> getLeft() {
        if (root == null) {
            throw new EmptyCollectionException("BinaryTree");
        }
        LinkedBinaryTree<T> result = new LinkedBinaryTree<>();
        result.root = root.getLeft();
        return result;
    }


    public LinkedBinaryTree<T> getRight() {
        if (root == null) {
            throw new EmptyCollectionException("BinaryTree");
        }
        LinkedBinaryTree<T> result = new LinkedBinaryTree<>();
        result.root = root.getRight();
        return result;
    }

    public void removeRightSubtree() {
        root.right = null;
    }

    public void removeAllElements() {
        root = null;
    }

    @Override
    public boolean isEmpty() {
        return (root == null);
    }

    @Override
    public int size() {
        int size = 0;
        BinaryTreeNode temp = root;
        BinaryTreeNode temp1 = root;
        while (temp.getLeft() != null) {
            temp = temp.left;
            size++;
        }

        while (temp1.getRight() != null) {
            temp1 = temp1.right;
            size++;
        }
        return size;
    }

    public int getHeight() {

        int result = height(root);
        return result;
    }

    private int height(BinaryTreeNode<T> node) {
        if (node == null) {
            return 0;
        }

        int hleft = height(node.getLeft());
        int hright = height(node.getRight());
        if (hleft > hright) {
            return ++hleft;
        } else {
            return ++hright;
        }
    }

    public int CountLeaf(BinaryTreeNode node) {
        int num1 = 0, num2 = 0;
        if (node == null) {
            return 0;
        }

        if (node.getRight() == null && node.getLeft() == null) {
            return 1;
        }

        if (node.getLeft() != null || node.getRight() != null) {
            num1 = CountLeaf(node.getLeft());
            num2 = CountLeaf(node.getRight());
        }
        return (num1 + num2);
    }

    @Override
    public boolean contains(T targetElement) {
        BinaryTreeNode node = root;
        BinaryTreeNode temp = root;
        boolean result = false;

        if (node == null) {
            result = false;
        }
        if (node.getElement().equals(targetElement)) {
            result = true;
        }
        while (node.right != null) {
            if (node.right.getElement().equals(targetElement)) {
                result = true;
                break;
            } else {
                node = node.right;
            }
        }
        while (temp.left.getElement().equals(targetElement)) {
            if (temp.left.getElement().equals(targetElement)) {
                result = true;
                break;
            } else {
                temp = temp.left;
            }
        }
        return result;
    }

    @Override
    public T find(T targetElement) throws ElementNotFoundException {
        BinaryTreeNode<T> current = findNode(targetElement, root);

        if (current == null) {
            throw new ElementNotFoundException("LinkedBinaryTree");
        }

        return (current.getElement());
    }

    private BinaryTreeNode<T> findNode(T targetElement,
                                       BinaryTreeNode<T> next) {
        if (next == null) {
            return null;
        }

        if (next.getElement().equals(targetElement)) {
            return next;
        }

        BinaryTreeNode<T> temp = findNode(targetElement, next.getLeft());

        if (temp == null) {
            temp = findNode(targetElement, next.getRight());
        }

        return temp;
    }

    public String toString2() {
        UnorderedListADT<BinaryTreeNode<T>> nodes =
                new ArrayUnorderedList<BinaryTreeNode<T>>();
        UnorderedListADT<Integer> levelList =
                new ArrayUnorderedList<Integer>();
        BinaryTreeNode<T> current;
        String result = "";
        int printDepth = this.getHeight();
        int possibleNodes = (int) Math.pow(2, printDepth + 1); // 最多的结点数 + 1
        int countNodes = 0;

        nodes.addToRear(root);
        Integer currentLevel = 0;
        Integer previousLevel = -1;
        levelList.addToRear(currentLevel);

        while (countNodes < possibleNodes) {
            countNodes = countNodes + 1;
            current = nodes.removeFirst();
            currentLevel = levelList.removeFirst();
            if (currentLevel > previousLevel) {
                result = result + "\n\n";
                previousLevel = currentLevel;
                for (int j = 0; j < ((Math.pow(2, (printDepth - currentLevel))) - 1); j++)
                    result = result + " ";
            } else {
                for (int i = 0; i < ((Math.pow(2, (printDepth - currentLevel + 1)) - 1)); i++) {
                    result = result + " ";
                }
            }
            if (current != null) {
                result = result + (current.getElement()).toString();
                nodes.addToRear(current.getLeft());
                levelList.addToRear(currentLevel + 1);
                nodes.addToRear(current.getRight());
                levelList.addToRear(currentLevel + 1);
            } else {
                nodes.addToRear(null);
                levelList.addToRear(currentLevel + 1);
                nodes.addToRear(null);
                levelList.addToRear(currentLevel + 1);
                result = result + " ";
            }

        }

        return result;
    }


    @Override
    public String toString() {
        UnorderedListADT<BinaryTreeNode<Integer>> nodes =
                new ArrayUnorderedList<BinaryTreeNode<Integer>>();
        UnorderedListADT<Integer> levelList =
                new ArrayUnorderedList<Integer>();
        BinaryTreeNode<Integer> current;
        String result = "";
        int printDepth = this.getHeight();
        int possibleNodes = (int) Math.pow(2, printDepth + 1);
        int countNodes = 0;

        nodes.addToRear((BinaryTreeNode<Integer>) root);
        Integer currentLevel = 0;
        Integer previousLevel = -1;
        levelList.addToRear(currentLevel);

        while (countNodes < possibleNodes) {
            countNodes = countNodes + 1;
            current = nodes.removeFirst();
            currentLevel = levelList.removeFirst();
            if (currentLevel > previousLevel) {
                result = result + "\n\n";
                previousLevel = currentLevel;
                for (int j = 0; j < ((Math.pow(2, (printDepth - currentLevel))) - 1); j++) {
                    result = result + " ";
                }
            } else {
                for (int i = 0; i < ((Math.pow(2, (printDepth - currentLevel + 1)) - 1)); i++) {
                    result = result + " ";
                }
            }
            if (current != null) {
                result = result + (current.getElement()).toString();
                nodes.addToRear(current.getLeft());
                levelList.addToRear(currentLevel + 1);
                nodes.addToRear(current.getRight());
                levelList.addToRear(currentLevel + 1);
            } else {
                nodes.addToRear(null);
                levelList.addToRear(currentLevel + 1);
                nodes.addToRear(null);
                levelList.addToRear(currentLevel + 1);
                result = result + " ";
            }

        }

        return result;
    }

    @Override
    public Iterator<T> iterator() {
        return iteratorInOrder();
    }

    @Override
    public Iterator<T> iteratorInOrder() {
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        inOrder(root, tempList);

        return new TreeIterator(tempList.iterator());
    }

    public void inOrder(BinaryTreeNode<T> node,
                        ArrayUnorderedList<T> tempList) {
        if (node != null) {
            inOrder(node.getLeft(), tempList);
            tempList.addToRear(node.getElement());
            inOrder(node.getRight(), tempList);
        }
    }


    @Override
    public Iterator<T> iteratorPreOrder() {
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        preOrder(root, tempList);

        return new TreeIterator(tempList.iterator());
    }

    public ArrayUnorderedList preOrder() {
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        preOrder(root, tempList);

        return tempList;
    }

    protected void preOrder(BinaryTreeNode<T> node,
                            ArrayUnorderedList<T> tempList) {
        if (node != null) {
            tempList.addToRear(node.getElement());
            preOrder(node.getLeft(), tempList);
            preOrder(node.getRight(), tempList);
        }
    }

    @Override
    public Iterator<T> iteratorPostOrder() {
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<>();
        postOrder(root, tempList);
        return new TreeIterator(tempList.iterator());
    }

    public ArrayUnorderedList postOrder() {
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        postOrder(root, tempList);

        return tempList;
    }

    protected void postOrder(BinaryTreeNode<T> node,
                             ArrayUnorderedList<T> tempList) {
        if (node != null) {
            postOrder(node.getLeft(), tempList);
            postOrder(node.getRight(), tempList);
            tempList.addToRear(node.getElement());
        }
    }

    @Override
    public Iterator<T> iteratorLevelOrder() {
        ArrayUnorderedList<BinaryTreeNode<T>> nodes =
                new ArrayUnorderedList<BinaryTreeNode<T>>();
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        BinaryTreeNode<T> current;

        nodes.addToRear(root);

        while (!nodes.isEmpty()) {
            current = nodes.removeFirst();

            if (current != null) {
                tempList.addToRear(current.getElement());
                if (current.getLeft() != null) {
                    nodes.addToRear(current.getLeft());
                }
                if (current.getRight() != null) {
                    nodes.addToRear(current.getRight());
                }
            } else {
                tempList.addToRear(null);
            }
        }

        return new TreeIterator(tempList.iterator());
    }


    private class TreeIterator implements Iterator<T> {
        private int expectedModCount;
        private Iterator<T> iter;


        public TreeIterator(Iterator<T> iter) {
            this.iter = iter;
            expectedModCount = modCount;
        }


        @Override
        public boolean hasNext() throws ConcurrentModificationException {
            if (!(modCount == expectedModCount)) {
                throw new ConcurrentModificationException();
            }

            return (iter.hasNext());
        }


        @Override
        public T next() throws NoSuchElementException {
            if (hasNext()) {
                return (iter.next());
            } else {
                throw new NoSuchElementException();
            }
        }


        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

}
