package tree3;

import java.util.Scanner;

public class PaixushuTest {
    public static void main(String[] args) {

        Node1 root = new Node1(new Data(50));
        Paixushu bst = new Paixushu(root);
        bst.insert(new Data(99));
        bst.insert(new Data(20));
        bst.insert(new Data(9));
        bst.insert(new Data(22));
        bst.insert(new Data(2));

        System.out.print("中序遍历结果为： ");
        System.out.println(bst);
        System.out.println("查找的数为：");
        Scanner scan = new Scanner(System.in);
        Data target = new Data();
        Integer data = scan.nextInt();
        target.data(data);
        boolean ifExist = bst.contains(target, root);

        System.out.println(ifExist);
        System.out.println("你想查找的数为：");
        Data target2 = new Data();
        Integer data2 = scan.nextInt();
        target2.data(data2);
        boolean ifExist2 = bst.contains(target2, root);
        System.out.println(ifExist2);
        System.out.println("你想插入的数是： ");
        Integer data3 = scan.nextInt();
        bst.insert(new Data(data3));
        System.out.println("中序遍历结果为： " + bst);
        System.out.println("你想插入的数是： ");
        Integer data4 = scan.nextInt();
        bst.insert(new Data(data4));
        System.out.println("中序遍历结果为： " + bst);
        System.out.println("删除一个节点：");
        bst.remove(new Data(2));
        System.out.println(bst);

    }
}
