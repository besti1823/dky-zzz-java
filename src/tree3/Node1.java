package tree3;

public class Node1 {
    public Data element;
    public Node1 left;
    public Node1 right;
    public int frequency = 1;

    public Node1(Data n) {
        this.element = n;
    }
}
