package tree3;


class Tnode {
    private int data;
    private Tnode lchild;
    private Tnode rchild;

    public Tnode(int data) {
        this.data = data;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public Tnode getLchild() {
        return lchild;
    }

    public void setLchild(Tnode lchild) {
        this.lchild = lchild;
    }

    public Tnode getRchild() {
        return rchild;
    }

    public void setRchild(Tnode rchild) {
        this.rchild = rchild;
    }

    @Override
    public String toString() {
        return "TreeNode [data=" + data + ", lchild=" + lchild + ", rchild=" + rchild + "]";
    }

}
