package tree3;

public class TestTree {
    public static void main(String[] args) {


        BinaryTreeNode node1 = new BinaryTreeNode(20);
        BinaryTreeNode node2 = new BinaryTreeNode(18);
        BinaryTreeNode node3 = new BinaryTreeNode(230);
        BinaryTreeNode node4 = new BinaryTreeNode(4);
        BinaryTreeNode node5 = new BinaryTreeNode(2018);
        BinaryTreeNode node6 = new BinaryTreeNode(2304);
        BinaryTreeNode node7 = new BinaryTreeNode(20182304);


        LinkedBinaryTree tree1 = new LinkedBinaryTree(node1.getElement());
        LinkedBinaryTree tree2 = new LinkedBinaryTree(node2.getElement());
        LinkedBinaryTree tree3 = new LinkedBinaryTree(node3.getElement());
        LinkedBinaryTree tree4 = new LinkedBinaryTree(node4.getElement());
        LinkedBinaryTree tree5 = new LinkedBinaryTree(node5.getElement(), tree1, tree2);
        LinkedBinaryTree tree6 = new LinkedBinaryTree(node6.getElement(), tree3, tree4);
        LinkedBinaryTree tree7 = new LinkedBinaryTree(node7.getElement(), tree5, tree6);

        //测试
        System.out.println("输出树：");
        System.out.println(tree7.toString());
        System.out.println("输出右子树：");
        System.out.println(tree7.getRight());
        System.out.println("输出左子树：");
        System.out.println(tree7.getLeft());
        System.out.print("是否含有数字2020:");
        System.out.println(tree7.contains(2020));
        System.out.println(("输出树的深度" + tree7.getHeight()));
        System.out.print("先序遍历 ");
        System.out.println(tree7.preOrder());
        System.out.print("\n后序遍历： ");
        System.out.println(tree7.postOrder());
    }
}