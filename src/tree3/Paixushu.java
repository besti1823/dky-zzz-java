package tree3;

public class Paixushu {
    private Node1 root;
    private int size;

    public Paixushu(Node1 root) {
        this.root = root;
        size++;
    }

    public int getSize() {
        return this.size;
    }

    public boolean contains(Node1 data) {
        return contains(this.root);
    }

    boolean contains(Data n, Node1 root) {
        if (root == null) {
            return false;
        }
        int compare = n.compareTo(root.element);
        if (compare > 0) {
            if (root.right != null) {
                return contains(n, root.right);
            } else {
                return false;
            }
        } else if (compare < 0) {
            if (root.left != null) {
                return contains(n, root.left);
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public boolean insert(Data n) {
        boolean flag = insert(n, this.root);
        if (flag) size++;
        return flag;
    }

    private boolean insert(Data n, Node1 root) {
        if (root == null) {
            this.root = new Node1(n);
            return true;
        } else if (root.element.compareTo(n) >= 0) {
            if (root.left != null) {
                return insert(n, root.left);
            } else {
                root.left = new Node1(n);
                return true;
            }
        } else if (root.element.compareTo(n) < 0) {
            if (root.right != null) {
                return insert(n, root.right);
            } else {
                root.right = new Node1(n);
                return true;
            }
        } else {
            root.frequency++;
            return true;
        }
    }

    public boolean remove(Data name) {
        root = remove(name, this.root);
        if (root != null) {
            size--;
            return true;
        }
        return false;
    }

    private Node1 remove(Data name, Node1 root) {
        int compare = root.element.compareTo(name);
        if (compare == 0) {
            if (root.frequency > 1) {
                root.frequency--;
            } else {
                if (root.left == null && root.right == null) {
                    root = null;
                } else if (root.left != null && root.right == null) {
                    root = root.left;
                } else if (root.left == null && root.right != null) {
                    root = root.right;
                } else {
                    Node1 newRoot = root.left;
                    while (newRoot.left != null) {
                        newRoot = newRoot.left;//找到左子树的最大节点
                    }
                    root.element = newRoot.element;
                    root.left = remove(root.element, root.left);
                }
            }
        } else if (compare > 0) {
            if (root.left != null) {
                root.left = remove(name, root.left);
            } else {
                return null;
            }
        } else {
            if (root.right != null) {
                root.right = remove(name, root.right);
            } else {
                return null;
            }
        }
        return root;
    }

    public String toString() {
        return toString(root);
    }

    private String toString(Node1 n) {
        String result = "";
        if (n != null) {
            if (n.left != null) {
                result += toString(n.left);
            }
            result += n.element + " ";
            if (n.right != null) {
                result += toString(n.right);
            }
        }
        return result;
    }

}
