import java.io.*;

public class read1 {
    public static void main(String[] args) throws IOException {

        File file = new File("C:\\Users\\666\\IdeaProjects\\dky-zzz-java", "HelloWorld2.txt");
        //File file = new File("HelloWorld.txt");
//        File file1 = new File("C:\\Users\\besti\\Desktop\\FileTest\\Test\\Test");
        if (!file.exists()) {
            file.createNewFile();
        }
        // file.delete();
        //文件操作：创造新文件
        OutputStream outputStream1 = new FileOutputStream(file);//输入，指向文件
        byte[] hello = {'H', 'e', 'l', 'l', 'o', ',', 'W', 'o', 'r', 'l', 'd', '!', '!', '!', '!', '!'};
        outputStream1.write(hello);//按字节写入
        outputStream1.flush();//刷新，第一种写法

        InputStream inputStream1 = new FileInputStream(file);//第一种读法，一个一个字节读取
        while (inputStream1.available() > 0) {
            System.out.print((char) inputStream1.read() + "  ");//区别？
        }
        inputStream1.close();
        System.out.println("\n成功按位读取。。");


    }
}