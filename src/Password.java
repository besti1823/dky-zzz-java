public interface Password {
    public void encrypt();

    public String decrypt();
}
