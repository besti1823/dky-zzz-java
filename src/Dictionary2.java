public class Dictionary2 extends Book2
{
    private int definitions;


    public Dictionary2(int  pages,int numDefinitions)
    {
        super(pages);//指向父类的成员，赋值,调用父类的构造方法
       definitions = numDefinitions;
    }
    public double computeRatio()
    {
        return definitions/pages;
    }

    public void setDefinitions(int numDefinitions) {
        this.definitions = numDefinitions;
    }

    public int getDefinitions() {
        return definitions;
    }
}
