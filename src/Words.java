public class Words {
    public static void main(String[] args) {
        Dictionary webster = new Dictionary();
        webster.setDefinition(15000);
        System.out.println("Number of pages: " + webster.getPages());
        System.out.println("Number of definitions: " + webster.getDefinition());
        System.out.println("Definitions per pages: " + webster.computeRatio());
    }
}
