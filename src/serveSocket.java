import java.io.*;
import java.net.Socket;

public class serveSocket {
    public static void main(String[] args) throws IOException {
        //1.建立客户端Socket连接，指定服务器位置和端口
        Socket socket = new Socket("172.16.43.142",8800);
//        Socket socket = new Socket("172.16.43.187",8800);

        //2.得到socket读写流
        OutputStream outputStream = socket.getOutputStream();
        //       PrintWriter printWriter = new PrintWriter(outputStream);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
        //输入流
        InputStream inputStream = socket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"GBK"));
        //3.利用流按照一定的操作，对socket进行读写操作
        String info1 = " 用户名：Sjw， 密码：20182302";
//        String info = new String(info1.getBytes("GBK"),"GBK");
        //     printWriter.write(info);
        //     printWriter.flush();
        outputStreamWriter.write(info1);
        outputStreamWriter.flush();
        socket.shutdownOutput();
        //接收服务器的响应
        String reply = null;
        while (!((reply = bufferedReader.readLine()) ==null)){
            System.out.println("用户信息: " + reply);
        }
        //4.关闭资源
        bufferedReader.close();
        inputStream.close();
        outputStreamWriter.close();
        //printWriter.close();
        outputStream.close();
        socket.close();
    }
}
