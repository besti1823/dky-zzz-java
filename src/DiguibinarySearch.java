public class DiguibinarySearch {
    public static void main(String[] args) {

        Searching search1 = new Searching();
        Comparable[] num = {0, 11, 24, 30, 41, 58, 63, 72, 83, 99};
        Comparable[] num1 = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

        int min = 0, max = 9, target = 72;

        System.out.println("分别用三种方法实现。");

        System.out.println("能否使用线性查找查找到72?");
        System.out.println("结果： " + search1.binarySearch(num, target));

        System.out.println("能否使用二分法查找到72?");
        System.out.println("结果： " + search1.binarySearch(num, target));

        System.out.println("能否使用二分法递归查找到72?");
        System.out.println("结果： " + search1.binarySearch2(num, min, max, target));


    }

}
