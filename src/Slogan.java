public class Slogan {
    private String phrase;
    private static int count = 0;
    public Slogan (String str)
    {
        phrase = str;
        count++;

    }

    @Override
    public String toString() {
        return phrase;
    }

    public static int getCount()
    {
        return count;
    }
}
