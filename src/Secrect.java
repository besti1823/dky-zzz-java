import java.util.Random;

public class Secrect implements Encryptable {
    private String message;
    private boolean encrypted;
    private int shift;
    private Random generator;

    public Secrect(String msg) {
        message = msg;
        encrypted = false;
        generator = new Random();
        shift = generator.nextInt(10) + 5;
    }

    public void encrypt() {
        if (!encrypted) {
            String masked = "";
            int index = 0;
            for (index = 0; index < message.length(); index++)
                masked = masked + (char) (message.charAt(index) + shift);
            message = masked;
            encrypted = true;
        }


    }

    public String decrypt() {
        if (encrypted) {
            String unmasked = "";
            int index = 0;
            for (index = 0; index < message.length(); index++)
                unmasked = unmasked + (char) (message.charAt(index) - shift);
            message = unmasked;
            encrypted = false;


        }
        return message;
    }

    public boolean isEncrypted() {
        return encrypted;
    }

    public String toString() {
        return message;
    }

}
