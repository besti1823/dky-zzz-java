public class Coin3 {
    public static void main(String[] args) {
        final int GOAL = 3;
        int count1 = 0;
        int count2 = 0;
        Coin2 coin1 = new Coin2();
        Coin2 coin2 = new Coin2();
        while (count1<GOAL&&count2<GOAL) {
            coin1.flip();
            coin2.flip();

            System.out.println("Coin 1: " + coin1 +
                    "  Coin 2: " + coin2);
            count1 = (coin1.isHeads()== Coin2.Con.HEADS) ? count1+1 : 0;
            count2 = (coin2.isHeads()== Coin2.Con.HEADS) ? count2+1 : 0;
        }
        if(count1 <GOAL)
        {
            System.out.println("Coin 2 wins!!!");

        }
        else
        if (count2<GOAL)
        {
            System.out.println("Coin 1 wins!!!");
        }
        else
        {
            System.out.println("It`s a Tie!!!");
        }

    }
}
