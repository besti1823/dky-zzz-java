public class Searching {
    public static Comparable linearSearch(Comparable[] data, Comparable target) {
        Comparable result = null;
        int index = 0;
        while (result == null && index < data.length) {
            if (data[index].compareTo(target) == 0)
                result = data[index];
            index++;
        }
        return result;

    }

    public static Comparable binarySearch(Comparable[] data, Comparable target) {
        Comparable result = null;
        int first = 0, last = data.length - 1, mid;
        while (result == null && first <= last) {
            mid = (first + last) / 2;
            if (data[mid].compareTo(target) == 0)
                result = data[mid];
            else if (data[mid].compareTo(target) > 0)
                last = mid - 1;
            else
                first = mid + 1;
        }
        return result;

    }

    //递归
    public static boolean binarySearch2(Comparable[] data, int min, int max, int target)
    /*public static Comparable binarySearch2(Comparable[] data,int min ,int max ,Comparable target)*/ {
        boolean found = false;
        int mpoint = (min + max) / 2;
        if (data[mpoint].compareTo(target) == 0)
            found = true;
        else if (data[mpoint].compareTo(target) > 0) {

            if (min < mpoint)
                found = binarySearch2(data, min, mpoint - 1, target);
        } else if (max > mpoint)
            found = binarySearch2(data, mpoint + 1, max, target);

        return found;


    }
}