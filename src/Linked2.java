

public class Linked2<T> {
    public LinearNode<T> head = new LinearNode<>();
    public LinearNode<T> temp = new LinearNode<>();

    public Linked2() {

        head = temp;

    }

    public void add(T elemt)  //添加某一元素
    {
        LinearNode<T> c = new LinearNode<T>(elemt);
        temp.setNext(c);
        temp = c;
    }

    public void insert(int a, T b) {
        LinearNode<T> top = new LinearNode<>();
        temp = head;//从头开始找
        for (int i = 0; i < a; i++) {

            top = temp;
            temp = temp.getNext();
        }
        LinearNode<T> c = new LinearNode<T>(b);
        top.setNext(c);
        c.setNext(temp);
        temp = top;//没用？

    }

    public void delete(int a) {

        temp = head;
        for (int i = 1; i < a; i++) {
            temp = temp.getNext();
        }
        LinearNode<T> c = new LinearNode<T>();
        c = temp;
        temp = temp.getNext();
        temp = temp.getNext();
        c.setNext(temp);
        temp = c;
    }

    public int isEmpty() {
        temp = head.getNext();
        int count = 0;
        while (temp != null) {
            temp = temp.getNext();
            count++;
        }
        return count;
    }

    public void Choose(int count) {
        temp = head.getNext();
        LinearNode<T> c;
        c = head.getNext();

        for (int i = 1; i <= count; i++) {

            for (int j = i + 1; j <= count; j++) {
                c = c.getNext();
                if (((int) c.getElement()) > ((int) temp.getElement())) {
                    T t;
                    t = c.getElement();
                    c.setElement(temp.getElement());
                    temp.setElement(t);
                }

            }
            temp = temp.getNext();
            c = temp;
        }
    }

    /*   public void maopao(int count)
       {
           temp = head.getNext();
           LinearNode<T> c;
           LinearNode<T> d;
           c=head.getNext();
           d=head.getNext();
           for(int i = 0;i<count-1;i++)
           {
               c=temp;
               d=c.getNext();
               for(int j =0;j<count-i-1;j++)
               {

                   if(((int)c.getElement())<((int)d.getElement()))
                   {
                       T t;
                       t = c.getElement();
                       c.setElement(d.getElement());
                       d.setElement(t);
                   }
                   c=c.getNext();
                   d=d.getNext();
               }
           }
       }
   */
    @Override
    public String toString() {
        LinearNode<T> current = new LinearNode<T>();
        current = head.getNext();
        String result = "";
        while (current != null) {
            result += "   " + current.getElement();
            current = current.getNext();
        }
        return result;
    }
}
